<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EpaperPages extends Model
{
    protected $table = 'epaper_pages';
    protected $fillable = ['epaper_date','epaper_id','page_no','url','url_original'];

}
