<?php
/**
 * Created by Zaman
 * Date: 29/5/2017
 * Time: 11:13 AM
 */

namespace App\Libraries;

use App\Modules\Pilgrim\Models\Notification;
use App\Modules\Pilgrim\Models\PassportIssuePlace;
use App\Modules\Pilgrim\Models\PilgrimNid;
use App\Modules\ProcessPath\Models\ProcessType;
use App\Modules\Settings\Models\Configuration;
use App\Modules\settings\Models\EmailQueue;
use App\Modules\Settings\Models\HajjSessions;
use App\Modules\Settings\Models\PilgrimListing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class Utility
{
    public static function isAllowForBulkApprovenet($user_type, $desk_list)
    {
        if (!$desk_list) {
            return false;
        }
        $processtype = ProcessType::where('id', 8)
            ->where('active_menu_for', 'like', "%$user_type%")->where('status', 1)->count();

        if ($processtype == 0) {
            return false;
        }
        if (in_array(15, $desk_list) && in_array(16, $desk_list)) {
            return true;
        }
        return false;
    }

    /*
     * Check if house related image exist or not in house_image table
     */
    public static function isHouseImageExist($house_id, $img_type = '')
    {
        $image_number = DB::table(env('HMIS_DB') . '.house_image')
            ->where('house_id', $house_id)
            ->where('img_type', $img_type)
            ->count();
        return $image_number > 0;
    }

    /*
     * Check if the guide already created password (Public page guide management utility)
     */
    public static function isAlreadyCreatedPassword($tracking_no, $session_id = '')
    {
        $is_exist = DB::table(env('HMIS_DB') . '.guide_search_otp')
            ->where('guide_track_no', $tracking_no)
            ->where('is_archived', 0)
            ->where('session_id', $session_id)
            ->count();
        return $is_exist > 0;
    }


    public static function dayIntervalByCurDate($date)
    {
        $now = time(); // or your date as well
        $given_date = strtotime($date);
        $datediff = $now - $given_date;
        return floor($datediff / (60 * 60 * 24));
    }


    public static function updatePassportNo($passport_no)
    {
        $passport_no = trim($passport_no);
        $base = substr($passport_no, 0, 2);
        $base = str_replace('0', 'O', $base);
        $base = str_replace('8', 'B', $base);
        $passportno = substr($passport_no, 2);
        $update_pp = $base . '' . $passportno;
        return $update_pp;
    }

    public static function getPassportPlaceData($pass_issue_place_id = 0, $pass_issue_place = '')
    {
        if ($pass_issue_place_id > 0) {
            $passport = PassportIssuePlace::where('id', $pass_issue_place_id)->first();
        } else if ($pass_issue_place != '') {
            $pass_issue_place = trim($pass_issue_place);
            $passport = PassportIssuePlace::where('place', $pass_issue_place)->first();
        }
        if ($passport != null) {
            $data['pass_issue_place'] = $passport->place;
            $data['pass_issue_place_id'] = $passport->id;
        } else {
            $data['pass_issue_place'] = '';
            $data['pass_issue_place_id'] = 0;
        }
        return $data;
    }

    public static function formatMobile($mobileNo)
    {
        return "<a href='tel:$mobileNo'>$mobileNo</a>";
    }

    public static function setMongoAccess($user_id = 0, $user_type = '')
    {
        $userTypes = ['1x101', '2x201', '2x202', '2x203', '2x205', '3x301', '3x302', '3x305', '3x306', '3x308', '10x411', '10x413', '10x414', '12x431', '12x432', '4x401', '4x402','11x421','11x422'];

        if (!in_array($user_type, $userTypes)) {
            return false;
        }

        $NID_BASE_URL = env('NID_BASE_URL');
        $REG_KEY = env('MONGO_NID_REG_KEY');
        $CLIENT_ID = env('CLIENT_ID');
        $USER_ID = $user_id;
        $url = $NID_BASE_URL . 'nid/api-request?param={"mongoDBRequest":{"requestData":{"reg_key":"' . $REG_KEY . '","clientId":"' . $CLIENT_ID . '","user_id":"' . $USER_ID . '"},"requestType":"LOGIN_REQUEST","version":"1.0"}}';

        $responses = @file_get_contents($url);
        $pecah = json_decode($responses);


        if (isset($pecah->mongoDBRequest->responseStatus->responseCode)) {
            $responseCode = intval($pecah->mongoDBRequest->responseStatus->responseCode);

            if ($responseCode == 1) {
                $authToken = $pecah->mongoDBRequest->responseStatus->responseData->auth_token;
                $auth_token = Encryption::encode($authToken);
                Session::put('client_key', $auth_token);

                return true;
            }
        }
        return false;
    }

    public static function isShowSBMessage($passport_no)
    {
        $passport_no = trim($passport_no);
        $session_id = HajjSessions::where('state', '=', 'active')->pluck('id');
        $HMIS_DB = env('HMIS_DB');

        $info = DB::table($HMIS_DB . '.flight_passenger_passports as fpp')
            ->join($HMIS_DB . '.flights as f', 'f.id', '=', 'fpp.flight_id')
            ->where('fpp.passport_no', $passport_no)
            ->where('fpp.session_id', $session_id)
            ->where('f.session_id', $session_id)
            ->first(['f.flight_status']);

        return (isset($info->flight_status) && ($info->flight_status == 'departed' || $info->flight_status == 'arrived')) ? false : true;
    }

    /**
     * In Pilgrim Listing, check: is SB Verification Status is active or not?
     * If not, then Message will not show
     */
    public static function isSBFieldInvestigationEnable($pilgrim_listing_id)
    {
        $sb_verification_status = PilgrimListing::where([
            'id' => $pilgrim_listing_id,
            'sb_verification' => 1
        ])->count();
        return ($sb_verification_status > 0) ? true : false;
    }

    public static function getNidSource($nid, $birthdate)
    {
        //$nid = Encryption::decode($nid);
        //$birthdate = Encryption::decode($birthdate);

        $nidData = DB::table('pilgrims_nid')->where('nid', 'like', $nid)->where('dob', '=', $birthdate)->whereIn('verification_flag', [1, 2])->select('responses', 'id')->first();

        if (!isset($nidData->id)) {
            $nidData = DB::table('nid_respons')->where('nid', 'like', $nid)->where('flag', 1)->select('responses', 'id')->first();

            if (!isset($nidData->id)) {
                $nidData = NIDVerification::getNidSource($nid, $birthdate);
                if (!$nidData) {
                    return 'NID Data not found';
                }
            }
        }
        try {
            $pecah = json_decode($nidData->responses);

            $str1 = "";
            $str2 = "";
            $tableS = "<table style='border:0px;width:100%;'>";
            $tableE = "</table>";
            $string = "Pilgrim Information has been updated!!!";

            if (isset($pecah->return) && $pecah->return) {
                foreach ($pecah->return as $key => $value) {
                    if ($key == "operationResult") {
                        continue;
                    }

                    $label = (string)$key;
                    $v = (string)$value;

                    if ($key == "photo") {
                        $nidPicture = 'data:image/jpeg;base64,' . $v;

                        $str2 .= "<tr>";
                        $str2 .= '<td><label>' . ucfirst($key) . '</label></td>';
                        $str2 .= '<td><img src="' . $nidPicture . '" alt="pilgrim image" class="profile-user-img img-responsive img-circle"/></td>';
                        $str2 .= "</tr>";
                        continue;
                    }


                    $str1 .= "<tr>";
                    $str1 .= '<td><label>' . ucfirst($label) . '</label></td>';
                    $str1 .= '<td>' . CommonFunction::convertUTF8($v) . '</td>';
                    $str1 .= "</tr>";
                }
                $string = $tableS . $str2 . $str1 . $tableE;
            }
        } catch (\Exception $e) {

            $string = "Something went wrong!!!" . $e->getMessage();
        }

        return $string;
    }

    public static function getRowDetailsNidData($created_by)
    {
        $NID_VERIFICATION_SOURCE = Configuration::where('caption', 'NID_VERIFICATION_SOURCE')->pluck('value');
        if ($NID_VERIFICATION_SOURCE == '1') {
            $query = PilgrimNid::orderBy('created_at', 'desc');
            if ($created_by != 0) {
                $query = $query->where('created_by', '=', $created_by);
            }
            $query = $query->orderBy('created_at', 'desc')
                ->orderBy('id', 'desc')
                ->limit(300);
            $pilgrimNid = $query->get(['id', 'nid', 'name', 'dob', 'verification_flag', 'submitted_at', 'no_of_try']);

        } else if ($NID_VERIFICATION_SOURCE == '2') {
            $url = NIDVerification::VERIFY_NID_URL('NA', 'NA', 'NA', 'list', $created_by);
            $responses = @file_get_contents($url);
            $responses = str_replace('"_id"', '"id"', $responses);
            $pecah = json_decode($responses);
            $responseCode = isset($pecah->mongoDBRequest->responseStatus->responseCode) ? intval($pecah->mongoDBRequest->responseStatus->responseCode) : 0;
            $responsesData = $pecah->mongoDBRequest->responseStatus->responseData;
            $pilgrimNid = ($responseCode == 200) ? $responsesData : null;
        }
        return $pilgrimNid;
    }

    public static function reSubmitNidStatus($id, $nid, $dob)
    {
        $NID_VERIFICATION_SOURCE = Configuration::where('caption', 'NID_VERIFICATION_SOURCE')->pluck('value');
        $response = false;
        if ($NID_VERIFICATION_SOURCE == '1') {
            $response = PilgrimNid::where('id', $id)->update(['verification_flag' => 0, 'no_of_try' => 0]);
        } else if ($NID_VERIFICATION_SOURCE == '2') {
            $VERIFICATION_FLAG = 0;
            $url = NIDVerification::VERIFY_NID_URL($nid, $dob, $VERIFICATION_FLAG, 'update');
            $responses = @file_get_contents($url);

            $pecah = json_decode($responses);
            $responseCode = isset($pecah->mongoDBRequest->responseStatus->responseCode) ? intval($pecah->mongoDBRequest->responseStatus->responseCode) : 0;
            $response = ($responseCode == 111) ? true : false;
        }
        return $response;
    }

    public static function getNidDataByNID($created_by, $nid)
    {
        $NID_VERIFICATION_SOURCE = Configuration::where('caption', 'NID_VERIFICATION_SOURCE')->pluck('value');
        $response = false;

        if ($NID_VERIFICATION_SOURCE == '1') {
            $query = PilgrimNid::orderBy('created_at', 'desc')
                ->where('nid', '=', $nid);
            if ($created_by != 0) {
                $query = $query->where('created_by', '=', $created_by);
            }
            $pilgrimNid = $query->get(['id', 'nid', 'name', 'dob', 'verification_flag', 'submitted_at', 'no_of_try']);
        } else if ($NID_VERIFICATION_SOURCE == '2') {
            $url = NIDVerification::VERIFY_NID_URL($nid, 'NA', 'NA', 'list', $created_by);
            $response = @file_get_contents($url);
            $response = str_replace('"_id"', '"id"', $response);
            $pecah = json_decode($response);
            $responseCode = isset($pecah->mongoDBRequest->responseStatus->responseCode) ? intval($pecah->mongoDBRequest->responseStatus->responseCode) : 0;
            $responsesData = $pecah->mongoDBRequest->responseStatus->responseData;
            $pilgrimNid = ($responseCode == 200) ? $responsesData : null;
        }
        return $pilgrimNid;
    }

    public static function requestAPI($data, $flag = 'insert')
    {
        if ($flag == 'insert') {
            $NID_BASE_URL = env('NID_BASE_URL');
            $USER_ID = Auth::user()->id;
            $AUTH_TOKEN = Encryption::decode(Session::get('client_key'));


            $data['mongoDBRequest']['requestData']['user_id'] = $USER_ID;
            $data['mongoDBRequest']['requestData']['auth_token'] = $AUTH_TOKEN;
            $data['mongoDBRequest']['requestType'] = 'SMS_LISTING_REQUEST';
            $data['mongoDBRequest']['version'] = 1.0;
            $param = urlencode(json_encode($data));
            $url = $NID_BASE_URL . 'sms/api-request?param=' . $param;


            $response = @file_get_contents($url);
            $pecah = json_decode($response);
            return isset($pecah->mongoDBRequest->responseStatus->responseCode) ? intval($pecah->mongoDBRequest->responseStatus->responseCode) : 0;
        }
        return 0;

    }

    /**
     * For CRON related task
     * @param $data
     * @param string $flag
     * @param $authToken
     * @return int
     */
    public static function CronRequestAPI($data, $flag = 'insert', $authToken)
    {
        if ($flag == 'insert') {
            $NID_BASE_URL = env('NID_BASE_URL');
            $USER_ID = 0;
            $AUTH_TOKEN = $authToken;


            $data['mongoDBRequest']['requestData']['user_id'] = $USER_ID;
            $data['mongoDBRequest']['requestData']['auth_token'] = $AUTH_TOKEN;
            $data['mongoDBRequest']['requestType'] = 'SMS_LISTING_REQUEST';
            $data['mongoDBRequest']['version'] = 1.0;
            $param = urlencode(json_encode($data));
            $url = $NID_BASE_URL . 'sms/api-request?param=' . $param;


            $response = @file_get_contents($url);
            $pecah = json_decode($response);
            return isset($pecah->mongoDBRequest->responseStatus->responseCode) ? intval($pecah->mongoDBRequest->responseStatus->responseCode) : 0;
        }
        return 0;

    }

    public static function submitSMS($smsData)
    {
        $SMS_SOURCE = Configuration::where('caption', 'SMS_SOURCE')->pluck('value');
        $response = false;

        // Mobile Number format update
        if (isset($smsData['destination'])) {
            $smsData['destination'] = Utility::updateMobileNo($smsData['destination']);
        }


        if ($SMS_SOURCE == '1') {
            Notification::create($smsData);
        } else if ($SMS_SOURCE == '2') {
            $data['mongoDBRequest']['requestData']['destination'] = isset($smsData['destination']) ? $smsData['destination'] : '';
            $data['mongoDBRequest']['requestData']['msg_type'] = isset($smsData['msg_type']) ? $smsData['msg_type'] : 'SMS';
            $data['mongoDBRequest']['requestData']['source'] = isset($smsData['source']) ? $smsData['source'] : '';
            $data['mongoDBRequest']['requestData']['ref_id'] = isset($smsData['ref_id']) ? $smsData['ref_id'] : 0;
            $data['mongoDBRequest']['requestData']['is_sent'] = isset($smsData['is_sent']) ? $smsData['is_sent'] : 0;
            $data['mongoDBRequest']['requestData']['template_id'] = isset($smsData['template_id']) ? $smsData['template_id'] : 0;
            $data['mongoDBRequest']['requestData']['priority'] = isset($smsData['priority']) ? $smsData['priority'] : 0;
            Utility::requestAPI($data, 'insert');
        }
    }

    /**
     * For Terminal Commands (CRON)
     * @param $smsData
     * @param $authToken
     */
    public static function CronSubmitSMS($smsData, $authToken)
    {
        $SMS_SOURCE = Configuration::where('caption', 'SMS_SOURCE')->pluck('value');
        $response = false;

        if ($SMS_SOURCE == '1') {
            Notification::create($smsData);
        } else if ($SMS_SOURCE == '2') {
            $data['mongoDBRequest']['requestData']['destination'] = isset($smsData['destination']) ? $smsData['destination'] : '';
            $data['mongoDBRequest']['requestData']['msg_type'] = isset($smsData['msg_type']) ? $smsData['msg_type'] : 'SMS';
            $data['mongoDBRequest']['requestData']['source'] = isset($smsData['source']) ? $smsData['source'] : '';
            $data['mongoDBRequest']['requestData']['ref_id'] = isset($smsData['ref_id']) ? $smsData['ref_id'] : 0;
            $data['mongoDBRequest']['requestData']['is_sent'] = isset($smsData['is_sent']) ? $smsData['is_sent'] : 0;
            $data['mongoDBRequest']['requestData']['template_id'] = isset($smsData['template_id']) ? $smsData['template_id'] : 0;
            $data['mongoDBRequest']['requestData']['priority'] = isset($smsData['priority']) ? $smsData['priority'] : 0;
            Utility::CronRequestAPI($data, 'insert', $authToken);
        }
    }


    public static function getUserLastNotificationInfo($mobile_no, $ref_id)
    {
        $SMS_SOURCE = Configuration::where('caption', 'SMS_SOURCE')->pluck('value');
        if ($SMS_SOURCE == '1') {
            $notificationInfo = Notification::where(['destination' => $mobile_no, 'ref_id' => $ref_id])
                ->orderBy('created_at', 'desc')
                ->first(['created_at']);
        } else if ($SMS_SOURCE == '2') {
            $is_sent = 'NA';
            $template_id = 'NA';
            $no_of_try = 'NA';
            $limit = 1;
            $searchedInfo = Utility::getSearchedSMSList($mobile_no, $ref_id, $is_sent, $template_id, $no_of_try, $limit);
            if ($searchedInfo) {
                $notify_time = date('Y-m-d H:i:s', $searchedInfo->created_at->sec);
                $notificationInfo = array();
                $notificationInfo['id'] = $searchedInfo->id;
                $notificationInfo['created_at'] = $notify_time;
                $notificationInfo = (object)$notificationInfo;
            }
        }

        return $notificationInfo;
    }

    public static function getSearchedSMSList($mobile_no, $ref_id, $is_sent = 'NA', $template_id = 'NA', $no_of_try = 'NA', $limit = 1)
    {
        $API_BASE_URL = env('NID_BASE_URL');
        if ($mobile_no != 'NA' || $mobile_no != '') {
            $mobile_no = Utility::updateMobileNo($mobile_no);
        }
        $user_id = Auth::user()->id;
        $AUTH_TOKEN = Encryption::decode(Session::get('client_key'));
        $requestData = [
            'destination' => $mobile_no,
            'msg_type' => 'SMS',
            'ref_id' => $ref_id,
            'is_sent' => $is_sent,
            'template_id' => $template_id,
            'user_id' => $user_id,
            'no_of_try' => $no_of_try,
            'limit' => $limit,
            'group_clients' => env('GROUP_CLIENTS'),
            'auth_token' => $AUTH_TOKEN,
        ];
        $requestParam = [
            'mongoDBRequest' => [
                'requestData' => $requestData,
                'requestType' => 'SMS_LIST_REQUEST',
                'version' => "1.0"
            ]
        ];
        $encoded_request = urlencode(json_encode($requestParam));
        $url = $API_BASE_URL . 'sms/api-request?param=' . $encoded_request;
        $responses = @file_get_contents($url);
        $responses = str_replace('"_id"', '"id"', $responses);
        $responses = str_replace('"createdat"', '"created_at"', $responses);
        $decodedResponse = json_decode($responses);
        $responseCode = isset($decodedResponse->mongoDBRequest->responseStatus->responseCode) ? intval($decodedResponse->mongoDBRequest->responseStatus->responseCode) : 0;
        $responsesData = $decodedResponse->mongoDBRequest->responseStatus->responseData;
        $result = ($responseCode == 200) ? $responsesData : null;
        return $result;
    }


    public static function updateMobileNo($mobileNo)
    {
        $mobileNo = trim($mobileNo);
        if (substr($mobileNo, 0, 3) == '+88') {
            $mobileNo = substr_replace($mobileNo, '88', 0, 3);
        } else if (substr($mobileNo, 0, 3) == '+96') {
            $mobileNo = substr_replace($mobileNo, '96', 0, 3);
        } else if (substr($mobileNo, 0, 5) == '00966') {
            $mobileNo = substr_replace($mobileNo, '966', 0, 5);
        }

        if ($two_digit = substr($mobileNo, 0, 2) == '01') {
            $mobileNo = '88' . $mobileNo;
        }
        if ($two_digit = substr($mobileNo, 0, 2) == '05') {
            $mobileNo = '966' . substr($mobileNo, 1, strlen($mobileNo));
        }

        return $mobileNo;
    }

    public static function updateNotificationRecord($record_id, $destination = 'NA', $ref_id = 'NA')
    {
        $API_BASE_URL = env('NID_BASE_URL');
        $USER_ID = Auth::user()->id;
        $destination = Utility::updateMobileNo($destination);
        $AUTH_TOKEN = Encryption::decode(Session::get('client_key'));
        $requestData = [
            'id' => $record_id,
            'source' => 'NA',
            'destination' => $destination,
            'msg_type' => 'NA',
            'ref_id' => $ref_id,
            'is_sent' => "0",
            'template_id' => 'NA',
            'user_id' => "$USER_ID",
            'no_of_try' => 'NA',
            'auth_token' => $AUTH_TOKEN,
        ];
        $requestParam = [
            'mongoDBRequest' => [
                'requestData' => $requestData,
                'requestType' => 'SMS_UPDATE_REQUEST',
                'version' => "1.0"
            ]
        ];
        $encoded_request = urlencode(json_encode($requestParam));
        $url = $API_BASE_URL . 'sms/api-request?param=' . $encoded_request;
        $responses = @file_get_contents($url);
        $decodedResponse = json_decode($responses);
        $responseCode = isset($decodedResponse->mongoDBRequest->responseStatus->responseCode) ? intval($decodedResponse->mongoDBRequest->responseStatus->responseCode) : 0;
        $result = ($responseCode == 200) ? 1 : 0;
        return $result;
    }

    public static function quickSMS($mobile_no, $code)
    {
        $robi_username = 'bus_auto';
        $robi_password = 'B@t@1998';
        $robi_from = 'Haj_IT';

        $message = urlencode('Security code for 2nd step login is: ' . $code);
        $mobile_no = str_replace("+88", "88", "$mobile_no");
        $requestUrl = "https://api.mobireach.com.bd/SendTextMultiMessage?Username=$robi_username&Password=$robi_password&From=$robi_from&To=$mobile_no&Message=$message";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        //$request = @simplexml_load_string($response);
        return $response;
    }


    public static function verifyOtpSMS($data)
    {
        $SMS_SOURCE = Configuration::where('caption', 'SMS_SOURCE')->pluck('value');
        if ($SMS_SOURCE == '1') {
            $otp_user_id = $data['ref_id'];
            $template_id = $data['template_id'];
            $otp_phone = $data['destination'];
            $minute = $data['buffer_interval'];
            $created_at = date("Y-m-d H:i:s", strtotime("-{$minute} minutes", time()));


            $result = Notification::where('destination', $otp_phone)
                ->where([
                    'ref_id' => $otp_user_id,
                    'template_id' => $template_id
                ])
                ->where('created_at', '>', $created_at)
                ->orderBy('created_at', 'desc')
                ->first();

            if ($result) // Its define that use has at least one SMS which is saved in 5 minutes - UPDATE existing SMS
            {
                $otp_code = explode(':', $result->source);
                $otp_code = trim($otp_code[1]);
                Notification::where('id', $result->id)->update(['is_sent' => 0, 'no_of_try' => 0]);
                return ['responseCode' => 1, 'otp_code' => $otp_code];
            } else // Its define that the user has not any sms which is save in 5 minutes - Insert new SMS
            {
                unset($data['buffer_interval']);
                Notification::where([
                    'destination' => $otp_phone,
                    'template_id' => $template_id,
                    'ref_id' => $otp_user_id
                ])->update(['is_sent' => 1]);
                Notification::create($data);
                $otp_code = explode(':', $data['source']);
                $otp_code = trim($otp_code[1]);
                return ['responseCode' => 1, 'otp_code' => $otp_code];
            }
        } else if ($SMS_SOURCE == '2') {
            $API_BASE_URL = env('NID_BASE_URL');
            $otp_phone = $data['destination'];
            $otp_phone = Utility::updateMobileNo($otp_phone);
            $user_id = Auth::user()->id;
            $auth_token = Encryption::decode(Session::get('client_key'));
            $otp_user_id = $data['ref_id'];
            $minute = $data['buffer_interval'];
            $otp_source = $data['source'];
            $is_sent = 0;
            $template_id = $data['template_id'];
            $priority = 0;
            $requestDataArr = [
                'destination' => $otp_phone,
                'msg_type' => 'SMS',
                'source' => $otp_source,
                'ref_id' => $otp_user_id,
                'is_sent' => $is_sent,
                'template_id' => $template_id,
                'priority' => $priority,
                'user_id' => $user_id,
                'time_intervel' => $minute,
                'inup_disabled' => 0,
                'auth_token' => $auth_token,
            ];
            $requestParam = [
                'mongoDBRequest' => [
                    'requestData' => $requestDataArr,
                    'requestType' => 'SECOND_STEP_REQUEST',
                    'version' => "1.0"
                ]
            ];
            $encoded_request = urlencode(json_encode($requestParam));
            $url = $API_BASE_URL . 'sms/api-request?param=' . $encoded_request;


            $responses = @file_get_contents($url);
            $decodedResponse = json_decode($responses);
            $responseCode = isset($decodedResponse->mongoDBRequest->responseStatus->responseCode) ? intval($decodedResponse->mongoDBRequest->responseStatus->responseCode) : 0;
            $result = ($responseCode == 200) ? 1 : 0;
            $otp_code = $decodedResponse->mongoDBRequest->responseStatus->responseData->code;
            return ['responseCode' => $result, 'otp_code' => $otp_code];
        }
    }

    public static function checkOtpValidity($otp_phone, $otp_user_id, $buffer_interval)
    {
        $SMS_SOURCE = Configuration::where('caption', 'SMS_SOURCE')->pluck('value');
        if ($SMS_SOURCE == '1') {
            $minute = $buffer_interval;
            $created_at = date("Y-m-d H:i:s", strtotime("-{$minute} minutes", time()));

            $result = Notification::where('destination', $otp_phone)
                ->where('ref_id', $otp_user_id)
                ->where('created_at', '>', $created_at)
                ->orderBy('created_at', 'desc')
                ->first();

            if ($result) {
                $otp_code = explode(':', $result->source);
                $otp_code = trim($otp_code[1]);
//                Notification::where('id',$result->id)->update(['is_sent'=>0,'no_of_try'=>0]);
                return ['responseCode' => 1, 'otp_code' => $otp_code];
            } else {
                return ['responseCode' => 0, 'otp_code' => ''];
            }

        } else if ($SMS_SOURCE == '2') {

            $API_BASE_URL = env('NID_BASE_URL');
            $otp_phone = Utility::updateMobileNo($otp_phone);
            $user_id = Auth::user()->id;
            $auth_token = Encryption::decode(Session::get('client_key'));
            $otp_source = 'NA';
            $is_sent = 0;
            $template_id = 0;
            $priority = 0;
            $requestDataArr = [
                'destination' => $otp_phone,
                'msg_type' => 'SMS',
                'source' => $otp_source,
                'ref_id' => $otp_user_id,
                'is_sent' => $is_sent,
                'template_id' => $template_id,
                'priority' => $priority,
                'user_id' => $user_id,
                'time_intervel' => $buffer_interval,
                'inup_disabled' => 1,
                'auth_token' => $auth_token,
            ];
            $requestParam = [
                'mongoDBRequest' => [
                    'requestData' => $requestDataArr,
                    'requestType' => 'SECOND_STEP_REQUEST',
                    'version' => "1.0"
                ]
            ];
            $encoded_request = urlencode(json_encode($requestParam));
            $url = $API_BASE_URL . 'sms/api-request?param=' . $encoded_request;
            $responses = @file_get_contents($url);
            $decodedResponse = json_decode($responses);
            $responseCode = isset($decodedResponse->mongoDBRequest->responseStatus->responseCode) ? intval($decodedResponse->mongoDBRequest->responseStatus->responseCode) : 0;
            $result = ($responseCode == 200) ? 1 : 0;
            if ($responseCode == 200) {
                $otp_code = $decodedResponse->mongoDBRequest->responseStatus->responseData->code;
                return ['responseCode' => $result, 'otp_code' => $otp_code];
            }
            return ['responseCode' => $result, 'otp_code' => ''];

        }
    }

    public static function sendEmailInQueue($emailData)
    {
        // Email queue ready
        try {
            $emailQueue = new EmailQueue();
            $emailQueue->email_type = $emailData['email_type'];
            $emailQueue->email_subject = $emailData['email_subject'];
            $emailQueue->email_to = $emailData['email_to'];
            $emailQueue->email_cc = $emailData['email_cc'];
            $emailQueue->email_body = $emailData['email_body'];
            $emailQueue->status = 0;
            $emailQueue->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get custom message
     * @param $exception_class
     * @param $error_code
     * @param $default_message
     * @return string
     */
    public static function eMsg($exception_class, $error_code = '001', $default_message = 'Something went wrong!')
    {
        $db_mode = Session::get("DB_MODE") == null ? 'UAT' : Session::get("DB_MODE");
        $error_message = $default_message;

        //Write in Log File
        Log::error($exception_class);

        if (!is_subclass_of($exception_class, 'Exception')) {
            return $error_message;
        }

        $error_message .= '[' . $error_code . ':' . strrev($exception_class->getLine()) . ']';
        if ($db_mode == 'UAT') {
            $error_message .= ', M:';
            $error_message .= $exception_class->getMessage() . ', F:' . $exception_class->getFile();
        }
        return $error_message;
    }

    /*
     * Pilgrim Listing Message showing function for public page search
     */
    public static function getPilgrimListingMessage($pilgrim_listing_id)
    {
        return PilgrimListing::where([
            'id' => $pilgrim_listing_id,
            'is_pilgrim_msg_published' => 1 // 1=Published
        ])->pluck('pilgrim_msg');
    }


    public static function verifyOTPCode($info)
    {
        $NID_BASE_URL = env('NID_BASE_URL');
        $AUTH_TOKEN = Encryption::decode(Session::get('client_key'));

        $data['mongoDBRequest']['requestData']['destination'] = $info['destination'];
        $data['mongoDBRequest']['requestData']['code'] = $info['code'];
        $data['mongoDBRequest']['requestData']['ref_id'] = $info['ref_id'];
        $data['mongoDBRequest']['requestData']['template_id'] = $info['template_id'];
        $data['mongoDBRequest']['requestData']['user_id'] = $info['user_id'];
        $data['mongoDBRequest']['requestData']['time_intervel'] = $info['time_intervel'];
        $data['mongoDBRequest']['requestData']['auth_token'] = $AUTH_TOKEN;
        $data['mongoDBRequest']['requestType'] = 'SMS_CODE_REQUEST';
        $data['mongoDBRequest']['version'] = '1.0';
        $param = urlencode(json_encode($data));
        #$param = json_encode($data);

        $url = $NID_BASE_URL . 'sms/api-request?param=' . $param;

        //dd($url);

        $response = @file_get_contents($url);
        $pecah = json_decode($response);
        return (isset($pecah->mongoDBRequest->responseStatus->responseCode) && $pecah->mongoDBRequest->responseStatus->responseCode == 200) ? $pecah->mongoDBRequest->responseStatus->responseData : 0;
    }
}

?>