<?php

namespace App\Libraries;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class BlockChainVerification
{
    private $clientID;
    private $regKey;
    private $version;
    private $blockChainBaseURL;

    public function __construct()
    {
        date_default_timezone_set('Asia/Dhaka');
        $this->blockChainBaseURL = env('BLOCKCHAIN_BASE_URL');
        $this->clientID = env('BLOCKCHAIN_CLIENT_ID');
        $this->regKey = env('BLOCKCHAIN_REG_KEY');
        $this->version = "1.0";
    }

    /**
     * Here, API request will generate to get Hash value.
     * @param $ref_data
     * @param $data
     * @param $hash
     * @return array
     */
    public function getHash($ref_data, $data, $hash)
    {
        try {

            $apiData = [
                "blockChainRequest" => [
                    "requestData" => [
                        "refData" => $ref_data,
                        "data" => $data,
                        "hash" => $hash
                    ],
                    "requestType" => "GET-HASH",
                    "version" => $this->version,
                    "clientId" => $this->clientID,
                    "regKey" => $this->regKey,
                ]
            ];
            $json_data = json_encode($apiData);
            $requestArr = [
                'form_params' => [
                    'param' => $json_data
                ]
            ];
            $client = new Client();
            $response = $client->post($this->blockChainBaseURL, $requestArr, ['verify' => false]);
            $responseCode = $response->getStatusCode();
            if ($responseCode == 200) {

                $responseBody = $response->getBody();
                $decodedResponse = json_decode($responseBody);
                $responseCode = $decodedResponse->blockChainResponse->responseCode;
                $responseMsg = $decodedResponse->blockChainResponse->message;

                if ($responseCode == 200) {

                    $responseData = $decodedResponse->blockChainResponse->responseData;
                    $previousHash = $responseData->previousHash;
                    $currentHash = $responseData->currentHash;
                    $apiResponseData['previousHash'] = $previousHash;
                    $apiResponseData['currentHash'] = $currentHash;

                    return ['responseCode' => $responseCode, 'message' => $responseMsg, 'data' => $apiResponseData];
                } else {
                    return ['responseCode' => $responseCode, 'message' => $responseMsg, 'data' => ''];
                }
            } else {
                return ['responseCode' => $responseCode, 'message' => 'API response can not be parsed.', 'data' => ''];
            }
        } catch (\Exception $e) {
            return ['responseCode' => 0, 'message' => 'Sorry, something went wrong.', 'data' => ''];
        }
    }

    /**
     * Here, BlockChain will be verified.
     * @param $ref_data
     * @param $data
     * @param $hash
     * @return array
     */
    public function verifyChain($ref_data, $data, $hash)
    {
        try {




            $apiData = [
                "blockChainRequest" => [
                    "requestData" => [
                        "refData" => $ref_data,
                        "data" => $data,
                        "hash" =>  ($hash == '') ? 'null' : $hash,
                    ],
                    "requestType" => "VERIFY-CHAIN",
                    "version" => $this->version,
                    "clientId" => $this->clientID,
                    "regKey" => $this->regKey,
                ]
            ];
//            echo "<pre>";
//            print_r($apiData);
//            echo "</pre>";

            $json_data = json_encode($apiData);
            $reqArr = [
                'form_params' => [
                    'param' => $json_data
                ]
            ];
//            dd($json_data);
//            dd($this->blockChainBaseURL);

            $client = new Client();
            $response = $client->request('POST', $this->blockChainBaseURL, $reqArr, ['verify' => false]);
            $responseCode = $response->getStatusCode();

            if ($responseCode == 200) {

                $responseBody = $response->getBody();
                $decodedResponse = json_decode($responseBody);
                $message = $decodedResponse->blockChainResponse->message;
                $responseCode = $decodedResponse->blockChainResponse->responseCode;

                if ($responseCode == 200) {

                    $responseData = $decodedResponse->blockChainResponse->responseData;
                    $isChainValid = $responseData->isChainValid;
                    return ['responseCode' => $responseCode, 'message' => $message, 'isValid' => $isChainValid];
                } else {
                    return ['responseCode' => $responseCode, 'message' => $message, 'isValid' => ''];
                }
            } else {
                return ['responseCode' => $responseCode, 'message' => 'API response can not be parsed.', 'isValid' => ''];
            }
        } catch (\Exception $e) {
            return ['responseCode' => 0, 'message' => 'Sorry, something went wrong.', 'isValid' => ''];
        }
    }

    /**
     * Here, API request will generate batch wise hash
     * @param $requestData
     * @return array
     */
    public function getBatchWiseHash($requestData)
    {
        try {
            $apiData = [
                "blockChainRequest" => [
                    "requestData" => $requestData,
                    "requestType" => "GET-BATCH-HASH",
                    "version" => $this->version,
                    "clientId" => $this->clientID,
                    "regKey" => $this->regKey,
                ]
            ];
            $json_data = json_encode($apiData);
            $requestArr = [
                'form_params' => [
                    'param' => $json_data
                ]
            ];
            $client = new Client();
            $response = $client->post($this->blockChainBaseURL, $requestArr, ['verify' => false]);
            $responseCode = $response->getStatusCode();
            if ($responseCode == 200) {

                $responseBody = $response->getBody();
                $decodedResponse = json_decode($responseBody);
                $responseCode = $decodedResponse->blockChainResponse->responseCode;
                $responseMsg = $decodedResponse->blockChainResponse->message;

                if ($responseCode == 200) {

                    $responseData = $decodedResponse->blockChainResponse->responseData;
                    return ['responseCode' => $responseCode, 'message' => $responseMsg, 'data' => $responseData];
                } else {
                    return ['responseCode' => $responseCode, 'message' => $responseMsg, 'data' => ''];
                }
            } else {
                return ['responseCode' => $responseCode, 'message' => 'API response can not be parsed.', 'data' => ''];
            }
        } catch (\Exception $e) {
            return ['responseCode' => 0, 'message' => 'Sorry, something went wrong.', 'data' => ''];
        }
    }

    /**
     * Verify batch wise hash
     * @param $requestData
     * @return array
     */
    public function verifyBatchWiseHash($requestData)
    {
        try {
            $apiData = [
                "blockChainRequest" => [
                    "requestData" => $requestData,
                    "requestType" => "VERIFY-BATCH-CHAIN",
                    "version" => $this->version,
                    "clientId" => $this->clientID,
                    "regKey" => $this->regKey,
                ]
            ];
            $json_data = json_encode($apiData);
            $reqArr = [
                'form_params' => [
                    'param' => $json_data
                ]
            ];
            $client = new Client();
            $response = $client->request('POST', $this->blockChainBaseURL, $reqArr, ['verify' => false]);
            $responseCode = $response->getStatusCode();

            if ($responseCode == 200) {

                $responseBody = $response->getBody();
                $decodedResponse = json_decode($responseBody);
                $message = $decodedResponse->blockChainResponse->message;
                $responseCode = $decodedResponse->blockChainResponse->responseCode;

                if ($responseCode == 200) {
                    //$responseData = $decodedResponse->blockChainResponse->responseData;
                    return ['responseCode' => $responseCode, 'message' => $message, 'isValid' => true];
                } else {
                    return ['responseCode' => $responseCode, 'message' => $message, 'isValid' => ''];
                }
            } else {
                return ['responseCode' => $responseCode, 'message' => 'API response can not be parsed.', 'isValid' => ''];
            }
        } catch (\Exception $e) {
            return ['responseCode' => 0, 'message' => 'Sorry, something went wrong.', 'isValid' => ''];
        }
    }

    /**
     * Is blockChain enabled
     * @return bool
     * @param $type
     */
    public static function isBlockChainEnabled($type = 'pre_reg_payment')
    {
        if ($type == 'pre_reg_payment') {// Pre Registration Payment
            return Configuration::where('caption', 'IS_PAYMENT_BLOCKCHAIN_ENABLED')->pluck('value') == '1';
        } elseif ($type == 'reg_payment') {// Registration Payment
            return Configuration::where('caption', 'IS_PAYMENT_BLOCKCHAIN_ENABLED')->pluck('value2') == '1';
        }
        return false;
    }

    /**
     * Verify and update batch wise hash
     * @param $bc_infos_before_update
     * @param $bc_ref_ids
     * @param $model
     * @param $ref_data_label
     * @param $ref_data_field
     * @return bool
     */
    public static function verifyAndUpdateBatchWiseHash($bc_infos_before_update, $bc_ref_ids, $model, $ref_data_label, $ref_data_field)
    {
        $blockChain = new BlockChain();
        $response = true;
        // Verify chain
        $is_verified = true;
        if (count($bc_infos_before_update) > 0) {

            // VERIFY-BATCH-CHAIN REQUEST
            $BatchWiseVerifyRequest = [];
            foreach ($bc_infos_before_update as $key => $info_before) {

                $c_hash = $info_before['current_hash'];
                unset($info_before['current_hash']);
                $BatchWiseVerifyRequest[] = [
                    'refData' => $ref_data_label . '-' . $info_before[$ref_data_field],
                    'data' => $info_before,
                    'hash' => $c_hash
                ];
            }
            $batchWiseHashResponse = $blockChain->verifyBatchWiseHash($BatchWiseVerifyRequest);
            if ($batchWiseHashResponse['responseCode'] != 200) {

                // Making error response
                $is_verified = false;
                $response = BlockChain::getBatchWiseErrorResponse($batchWiseHashResponse['message'], $ref_data_field);
            }
        }
        // GET-BATCH-HASH REQUEST
        if ($is_verified) {

            // Batch wise request format ready
            $bc_infos_after_update = $model::blockChainUpdateAbleData($bc_ref_ids, false);
            $BatchWiseHashRequest = [];
            foreach ($bc_infos_after_update as $key => $info_after) {
                $c_hash = $info_after['current_hash'];
                unset($info_after['current_hash']);
                $BatchWiseHashRequest[] = [
                    'refData' => $ref_data_label . '-' . $info_after[$ref_data_field],
                    'data' => $info_after,
                    'hash' => $c_hash
                ];
            }

            // GET-BATCH-HASH
            $batchWiseHashResponse = $blockChain->getBatchWiseHash($BatchWiseHashRequest);
            // In case of invalid response
            if ($batchWiseHashResponse['responseCode'] != 200) {
                return BlockChain::getBatchWiseErrorResponse($batchWiseHashResponse['message'], $ref_data_field);
            }
            // In case of valid response
            if ($batchWiseHashResponse['responseCode'] == 200) {
                foreach ($batchWiseHashResponse['data'] as $key => $singleHash) {
                    $model->where($ref_data_field, $singleHash->data->$ref_data_field)->update([
                        'previous_hash' => $singleHash->previousHash,
                        'current_hash' => $singleHash->currentHash
                    ]);
                }
                return $response;
            }
        }
        return $response;
    }

    public static function getBatchWiseErrorResponse($messages, $ref_data_field)
    {
        $response = '';
        $encrypted_issue_no = [];
        if (count($messages) > 0) {
            foreach ($messages as $msg_key => $message) {
                $encrypted_issue_no[] = Encryption::encode($message->error_in->data->$ref_data_field);
            }
            $response = implode(',', $encrypted_issue_no);
        }
        return $response;
    }
}