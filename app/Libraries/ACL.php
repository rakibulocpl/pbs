<?php

namespace App\Libraries;

use App\Modules\Apps\Models\P2ProcessList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ACL
{

    public static function db_reconnect()
    {
        if (Session::get('DB_MODE') == 'PRODUCTION') {
//        DB::purge('mysql-main');
//        DB::setDefaultConnection('mysql-main');
//        DB::setDefaultConnection(Session::get('mysql_access'));
        }
    }

    public static function hasUserModificationRight($userType, $right, $id)
    {
        try {
            $userId=CommonFunction::getUserId();
            if($userType=='1x101')
                return true;

            if($userId==$id)
                return true;

            return false;
        } catch (\Exception $e) {
            dd(CommonFunction::showErrorPublic($e->getMessage()));
            return false;
        }
    }


    public static function hasApplicationModificationRight($processType, $user_type, $right, $id)
    {
        try {
            if ($right != 'E')
                return true;
            $company_id = CommonFunction::getUserSubTypeWithZero();
            $data = P2ProcessList::where([
                'ref_id' => $id,
                'process_type_id' => $processType,
            ])
                ->first(['p2_process_list.process_status_id', 'p2_process_list.created_by']);
            if ($data->company_id == $company_id && in_array($data->process_status_id, [-1, 5])) {
                return true;
            } else {
                return false;
            }


        } catch (\Exception $e) {
            dd(CommonFunction::showErrorPublic($e->getMessage()));
            return false;
        }
    }

    public static function hasCertificateModificationRight($right, $id)
    {
        try {
            if ($right != 'E')
                return true;
            $info = UploadedCertificates::where('uploaded_certificates.doc_id', $id)->first(['company_id']);
            if ($info->company_id == Auth::user()->user_sub_type) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            dd(CommonFunction::showErrorPublic($e->getMessage()));
            return false;
        }
    }

    public static function getAccsessRight($module, $right = '', $id = null)
    {
        $accessRight = '';
        if (Auth::user()) {
            $user_type = Auth::user()->user_type;
        } else {
            die('You are not authorized user or your session has been expired!');
        }
        switch ($module) {
            case 'settings':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                } elseif ($user_type == '4x404') {
                $accessRight = 'AVE';
            }
                break;
            case 'Training':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                }
                break;
            case 'dashboard':
                if ($user_type == '1x101') {
                    $accessRight = 'AVESERN';
                } elseif ($user_type == '5x505') {
                    $accessRight = 'AVESERNH';
                } elseif ($user_type == '13x131') {
                    $accessRight = 'AVESERNH';
                }
                break;
            case 'faq':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                }else if($user_type == '2x202' ||$user_type == '2x205'){
                    $accessRight = 'V';
                }
                break;
            case 'search':
                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '2x203') {
                    $accessRight = 'AVE';
                } else if ($user_type == '3x300' || $user_type == '3x305') {
                    $accessRight = 'V';
                } else {

                }
                break;

            case 'report':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                } else if ($user_type == '5x505' || $user_type == '6x606') {
                    $accessRight = 'V';
                } else if ($user_type == '7x707') {
                    $accessRight = 'AVE';
                }else {
                    $accessRight = 'V';
                }
                break;

                case 'CustomNotification':
                    if ($user_type == '1x101') {
                        $accessRight = 'AVE';
                    }
                break;

            case 'user':
                if ($user_type == '1x101') {
                    $accessRight = '-A-V-E-R-';
                } else if ($user_type == '2x202') {
                    $accessRight = 'VER';
                } else if ($user_type == '4x404') {
                    $accessRight = '-V-R-';
                } else {
                    $accessRight = '-V-R-E';
                }
                if($right=="SPU"){
                    if (ACL::hasUserModificationRight($user_type, $right, $id))
                        return true;
                }

                break;
            case 'CompanyAssociation':
                if (in_array($user_type, ['1x101', '2x202'])) {
                    $accessRight = '-V-UP-';
                } elseif (in_array($user_type, ['5x505','5x506','13x131','15x151','6x606'])) {
                    $accessRight = '-A-V-';
                }
                break;
            case 'processPath':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                }
                break;

            case 'LocalPurchasePermit':

                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '7x707' || $user_type == '8x808') {
                    //7x707=Super and 8x808=Zone user, 9x909 Customs
                    $accessRight = '-V-';
                } else if (in_array($user_type ,['5x505','5x506','13x131','15x151','6x606'])) {
                    $accessRight = '-A-E-V-';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasApplicationModificationRight('11', $user_type, $right, $id) == false)
                            return false;
                    }
                } else if (in_array($user_type, ['3x303', '4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }

                break;

            case 'LocalSalesPermit':
                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '7x707' || $user_type == '8x808') {
                    //7x707=Super and 8x808=Zone user, 9x909 Customs
                    $accessRight = '-V-';
                } else if ($user_type == '5x505' || $user_type == '5x506' || $user_type == '13x131') {
                    $accessRight = '-A-E-V-';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasApplicationModificationRight('11', $user_type, $right, $id) == false)
                            return false;
                    }
                } else if (in_array($user_type, ['3x303', '4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'ProjectClearance':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;
            case 'ProjectClearanceAmendment':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'LandRequisition':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'ImportPermit':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;
            case 'Occupancy':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'ExportPermit':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'VisaAssistance':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'WorkPermit':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'SampleExportPermit':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'TradeLicense':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'CommercialOperation':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'LandUse':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'VisaRecommendation':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'SampleImportPermit':
                if (in_array($user_type, ['1x101', '2x202', '7x707', '8x808'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505', '13x131','3x303','15x151','5x506'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;


            case 'DOE':
                if (in_array($user_type, ['1x101'])) { //7x707=Super and 8x808=Zone user
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404'])) {
                    $accessRight = '-V-UP-';
                }
                break;


            case 'SonaliPayment':
                if ($user_type == '1x101') {
                    $accessRight = '-A-V-E-';
                } elseif ($user_type == '5x505') {
                    /*
                     * CPC = Counter Payment Cancel
                     * CPCR = Counter Payment Confirmation Request
                     */
                    $accessRight = '-CPC-CPCR-';
                }
                break;




            case 'CoBrandedCard':
                if (in_array($user_type,['1x101','7x707','8x808','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x909'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'LimitEnhancement':
                if (in_array($user_type, ['1x101','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-UP-';
                }else if (in_array($user_type, ['4x404'])) {
                    $accessRight = '-V-UP-';
                }
                break;




            case 'certificate':
                if ($user_type == '5x505' || $user_type == '6x606') { // 5x505 = Unit investors and 6x606 = VA users
                    $accessRight = 'AVE';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasCertificateModificationRight($right, $id) == false)
                            return false;
                    }
                } else if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                }
                break;

            case 'LicenceApplication':
                if (in_array($user_type,['1x101','2x202','7x707','8x808','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x901'])) {
                    $accessRight = '-V-UP-';
                }
                break;
            case 'NewReg':
                if (in_array($user_type,['1x101','2x202','7x707','8x808','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-';
                }else if (in_array($user_type, ['4x404', '6x606', '7x707', '9x901'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'NameClearance':
                if (in_array($user_type,['1x101','2x202','4x404','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-';
                }else if (in_array($user_type, ['9x901'])) {
                    $accessRight = '-V-UP-';
                }
                break;
            case 'CompanyRegistration':
                if (in_array($user_type,['1x101','2x202','4x404','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-';
                }else if (in_array($user_type, ['9x901'])) {
                    $accessRight = '-V-UP-';
                }
                break;


            case 'E-tin':
                if (in_array($user_type,['1x101','2x202','4x404','13x303'])) {
                    $accessRight = '-V-';
                } else if (in_array($user_type, ['5x505'])) {
                    $accessRight = '-A-V-E-';
                }else if (in_array($user_type, ['9x901'])) {
                    $accessRight = '-V-UP-';
                }
                break;

            case 'display-settings':
                if ($user_type == '1x101' || $user_type == '15x151') {
                    $accessRight = 'AVE';
                }
                break;

            default:
                $accessRight = '';
        }
        if ($right != '') {
            if (strpos($accessRight, $right) === false) {
                return false;
            } else {
                return true;
            }
        } else {
            return $accessRight;
        }
    }
    public static function hasProjectClearanceModificationRight($user_type, $right, $id) {
        try {
            if ($right != 'E') {
                return true;
            } else {
                $projectClearanceInfo = ProjectClearance::LeftJoin('process_list', function ($join) {
                    $join->on('process_list.record_id', '=', 'project_clearance.id');
                    $join->on('process_list.service_id', '=', DB::raw('1'));
                })
                    ->where('project_clearance.id', $id)
                    ->first(['process_list.status_id', 'project_clearance.company_id']);

                if ($projectClearanceInfo->company_id == Auth::user()->user_sub_type && in_array($projectClearanceInfo->status_id, [-1, 5])) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (\Exception $e) {
            dd(CommonFunction::showErrorPublic($e->getMessage()));
            return false;
        }
    }

    public static function isAllowed($accessMode, $right)
    {
        if (strpos($accessMode, $right) === false) {
            return false;
        } else {
            return true;
        }
    }

    /*     * **********************************End of Class****************************************** */
}
