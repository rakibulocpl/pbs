<?php


namespace App\Libraries;


class ConvertToUnicode
{
    public $bijoy_string_conversion_map = [
        // <JUKTAKHKHOR>
        "i¨"=>"র‌্য",
        "ª¨"=>"্র্য",
        "°"=>"ক্ক",
        "±"=>"ক্ট",
        "³"=>"ক্ত",
        "K¡"=>"ক্ব",
        "¯Œ"=>"স্ক্র",
        "µ"=>"ক্র",
        "K¬"=>"ক্ল",
        "ÿ"=>"ক্ষ",
        "•"=>"ক্স",
        "¸"=>"গু",
        "»"=>"গ্ধ",
        "Mœ"=>"গ্ন",
        "M¥"=>"গ্ম",
        "Mø"=>"গ্ল",
        "¼"=>"ঙ্ক",
        "•¶"=>"ঙ্ক্ষ",
        "•L"=>"ঙ্খ",
        "½"=>"ঙ্গ",
        "•N"=>"ঙ্ঘ",
        "•"=>"ক্স",
        "”P"=>"চ্চ",
        "”Q"=>"চ্ছ",
        "”Q¡"=>"চ্ছ্ব",
        "”T"=>"চ্ঞ",
        "¾¡"=>"জ্জ্ব",
        "¾"=>"জ্জ",
        "À"=>"জ্ঝ",
        "Á"=>"জ্ঞ",
        "R¡"=>"জ্ব",
        "Â"=>"ঞ্চ",
        "Ã"=>"ঞ্ছ",
        "Ä"=>"ঞ্জ",
        "Å"=>"ঞ্ঝ",
        "Æ"=>"ট্ট",
        "U¡"=>"ট্ব",
        "U¥"=>"ট্ম",
        "Ç"=>"ড্ড",
        "È"=>"ণ্ট",
        "É"=>"ণ্ঠ",
        "Ý"=>"ন্স",
        "Ð"=>"ণ্ড",
        "š‘"=>"ন্তু",
        "Y^"=>"ণ্ব",
        "Ë"=>"ত্ত",
        "Ë¡"=>"ত্ত্ব",
        "Ì"=>"ত্থ",
        "Z¥"=>"ত্ম",
        "šÍ¡"=>"ন্ত্ব",
        "Z¡"=>"ত্ব",
        "Î"=>"ত্র",
        "_¡"=>"থ্ব",
        "˜M"=>"দ্গ",
        "˜N"=>"দ্ঘ",
        "Ï"=>"দ্দ",
        "×"=>"দ্ধ",
        "˜¡"=>"দ্ব",
        "Ø"=>"দ্ব",
        "™¢"=>"দ্ভ",
        "Ù"=>"দ্ম",
        "`ªæ"=>"দ্রু",
        "aŸ"=>"ধ্ব",
        "a¥"=>"ধ্ম",
        "›U"=>"ন্ট",
        "Ú"=>"ন্ঠ",
        "Û"=>"ন্ড",
        "šÍ"=>"ন্ত",
        "šÍ"=>"ন্ত",
        "š¿"=>"ন্ত্র",
        "š’"=>"ন্থ",
        "›`"=>"ন্দ",
        "›Ø"=>"ন্দ্ব",
        "Ü"=>"ন্ধ",
        "bœ"=>"ন্ন",
        "š^"=>"ন্ব",
        "b¥"=>"ন্ম",
        "Þ"=>"প্ট",
        "ß"=>"প্ত",
        "cœ"=>"প্ন",
        "à"=>"প্প",
        "cø"=>"প্ল",
        "cø"=>"প্ল",
        "á"=>"প্স",
        "d¬"=>"ফ্ল",
        "â"=>"ব্জ",
        "ã"=>"ব্দ",
        "ä"=>"ব্ধ",
        "eŸ"=>"ব্ব",
        "eø"=>"ব্ল",
        "å"=>"ভ্র",
        "gœ"=>"ম্ন",
        "¤ú"=>"ম্প",
        "ç"=>"ম্ফ",
        "¤\\^"=>"ম্ব", //ম্ব ¤\\^ //¤^
        "¤¢"=>"ম্ভ",
        "¤£"=>"ম্ভ্র",
        "¤§"=>"ম্ম",
        "¤ø"=>"ম্ল",
        "iæ"=>"রু",
        "iæ"=>"রু",
        "iƒ"=>"রূ",
        "é"=>"ল্ক",
        "ê"=>"ল্গ",
        "ë"=>"ল্ট",
        "ì"=>"ল্ড",
        "í"=>"ল্প",
        "î"=>"ল্ফ",
        "j¦"=>"ল্ব",
        "j¥"=>"ল্ম",
        "jø"=>"ল্ল",
        "ï"=>"শু",
        "ð"=>"শ্চ",
        "kœ"=>"শ্ন",
        "k¦"=>"শ্ব",
        "k¥"=>"শ্ম",
        "kø"=>"শ্ল",
        "®‹"=>"ষ্ক",
        "®Œ"=>"ষ্ক্র",
        "ó"=>"ষ্ট",
        "ô"=>"ষ্ঠ",
        "ò"=>"ষ্ণ",
        "®ú"=>"ষ্প",
        "õ"=>"ষ্ফ",
        "®§"=>"ষ্ম",
        "¯‹"=>"স্ক",
        "÷"=>"স্ট",
        "ö"=>"স্খ",
        "¯Í"=>"স্ত",
        "¯‘"=>"স্তু",
        "¯¿"=>"স্ত্র",
        "¯’"=>"স্থ",
        "mœ"=>"স্ন",
        "¯ú"=>"স্প",
        "ù"=>"স্ফ",
        "¯^"=>"স্ব",
        "¯§"=>"স্ম",
        "mø"=>"স্ল",
        "û"=>"হু",
        "nè"=>"হ্ণ",
        "ý"=>"হ্ন",
        "þ"=>"হ্ম",
        "n¬"=>"হ্ল",
        "ü"=>"হৃ",
        "©"=>"র্",

        // <VOWELS>
        "Av"=>"আ",
        "A"=>"অ",
        "B"=>"ই",
        "C"=>"ঈ",
        "D"=>"উ",
        "E"=>"ঊ",
        "F"=>"ঋ",
        "G"=>"এ",
        "H"=>"ঐ",
        "I"=>"ও",
        "J"=>"ঔ",

        // <CONSONANTS>
        "K"=>"ক",
        "L"=>"খ",
        "M"=>"গ",
        "N"=>"ঘ",
        "O"=>"ঙ",
        "P"=>"চ",
        "Q"=>"ছ",
        "R"=>"জ",
        "S"=>"ঝ",
        "T"=>"ঞ",
        "U"=>"ট",
        "V"=>"ঠ",
        "W"=>"ড",
        "X"=>"ঢ",
        "Y"=>"ণ",
        "Z"=>"ত",
        "_"=>"থ",
        "`"=>"দ",
        "a"=>"ধ",
        "b"=>"ন",
        "c"=>"প",
        "d"=>"ফ",
        "e"=>"ব",
        "f"=>"ভ",
        "g"=>"ম",
        "h"=>"য",
        "i"=>"র",
        "j"=>"ল",
        "k"=>"শ",
        "l"=>"ষ",
        "m"=>"স",
        "n"=>"হ",
        "o"=>"ড়",
        "p"=>"ঢ়",
        "q"=>"য়",
        "r"=>"ৎ",

        // <DIGITS>
        "0"=>"০",
        "1"=>"১",
        "2"=>"২",
        "3"=>"৩",
        "4"=>"৪",
        "5"=>"৫",
        "6"=>"৬",
        "7"=>"৭",
        "8"=>"৮",
        "9"=>"৯",

        // Kars
        "v"=>"া",
        "w"=>"ি",
        "x"=>"ী",
        "y"=>"ু",
        "z"=>"ু",
        "~"=>"ূ",
        "„"=>"ৃ",
        "‡"=>"ে",
        "†"=>"ে",
        "‰"=>"ৈ",
        "ˆ"=>"ৈ",
        "Š"=>"ৗ",

        // signs
        "Ô"=>"‘",
        "Õ"=>"’",
        "\\|"=>"।",
        "Ò"=>"“",
        "Ó"=>"”",

        // Complex
        "s"=>"ং",
        "t"=>"ঃ",
        "u"=>"ঁ",
        "ª"=>"্র",
        "Ö"=>"্র",
        "«"=>"্র",
        "¨"=>"্য",
        "\\&"=>"্",
        "…"=>"ৃ"
    ]; // end bijoy_string_conversion_map
   public function customlen($a){
        return mb_strlen($a,'UTF-8');
    }

    public function customsubstr($a,$x,$y=null){
        if($y===NULL){
            $y=$this->customlen($a);
        }
        return mb_substr($a,$x,$y,'UTF-8');
    }

   public function customcharAt($a,$i){
        return $this->customsubstr($a,$i,1);
    }

    function IsBanglaDigit($CUni)
    {
        if($CUni=='০' || $CUni=='১'
            || $CUni=='২' || $CUni=='৩'
            || $CUni=='৪' || $CUni=='৫'
            || $CUni=='৬' || $CUni=='৭'
            || $CUni=='৮' || $CUni=='৯')
            return true;

        return false;
    } // end function IsBanglaDigit

    function IsBanglaPreKar($CUni)
    {
        if($CUni=='ি' || $CUni=='ৈ'
            || $CUni=='ে' )
            return true;

        return false;
    } // end function IsBanglaPreKar

    function IsBanglaPostKar($CUni)
    {
        if($CUni == 'া' || $CUni=='ো'
            || $CUni=='ৌ' || $CUni=='ৗ' || $CUni=='ু'
            || $CUni=='ূ' || $CUni=='ী'
            || $CUni=='ৃ')
            return true;
        return false;
    } // end function IsBanglaPostKar

    function IsBanglaKar($CUni)
    {
        if($this->IsBanglaPreKar($CUni) || $this->IsBanglaPostKar($CUni) )
            return true;
        return false;

    } // end function IsBanglaKar

    function IsBanglaBanjonborno($CUni)
    {
        if($CUni=='ক' || $CUni=='খ' || $CUni=='গ' || $CUni=='ঘ' || $CUni=='ঙ'
            || $CUni=='চ' || $CUni=='ছ' || $CUni=='জ' || $CUni=='ঝ' || $CUni=='ঞ'
            || $CUni=='ট' || $CUni=='ঠ' || $CUni=='ড' || $CUni=='ঢ' || $CUni=='ণ'
            || $CUni=='ত' || $CUni=='থ' || $CUni=='দ' || $CUni=='ধ' || $CUni=='ন'
            || $CUni=='প' || $CUni=='ফ' || $CUni=='ব' || $CUni=='ভ' || $CUni=='ম'
            || $CUni=='শ' || $CUni=='ষ' || $CUni=='স' || $CUni=='হ'
            || $CUni=='য' || $CUni=='র' || $CUni=='ল' || $CUni=='য়'
            || $CUni=='ং' || $CUni=='ঃ' || $CUni=='ঁ'
            || $CUni=='ৎ')
            return true;

        return false;
    } // end function IsBanglaBanjonborno

   public function IsBanglaSoroborno($CUni)
    {
        if($CUni == 'অ' || $CUni=='আ'
            || $CUni=='ই' || $CUni=='ঈ'
            || $CUni=='উ' || $CUni=='ঊ'
            || $CUni=='ঋ' || $CUni=='ঌ'
            || $CUni=='এ' || $CUni=='ঐ'
            || $CUni=='ও' || $CUni=='ঔ' )
            return true;

        return false;
    } // end function IsBanglaSoroborno

   public function IsBanglaNukta($CUni)
    {
        if($CUni=='ং' || $CUni=='ঃ' || $CUni=='ঁ')
            return true;

        return false;

    } // end function IsBanglaNukta

   public function IsBanglaFola($CUni)
    {
        if($CUni=="্য" || $CUni=="্র")
            return true;

        return false;
    } // end function IsBanglaFola

   public function IsBanglaHalant($CUni)
    {
        if($CUni=='্')
            return true;

        return false;
    } // end function IsBanglaHalant
   public function IsBangla($CUni)
    {
        if($this->IsBanglaDigit($CUni) || $this->IsBanglaKar($CUni) ||
            $this->IsBanglaBanjonborno($CUni) || $this->IsBanglaSoroborno($CUni) ||
            $this->IsBanglaNukta($CUni) || $this->IsBanglaFola($CUni) || $this->IsBanglaHalant($CUni))
            return true;

        return false;
    } // end function IsBangla
   public function IsASCII($CH)
    {
        if($CH >= 0 && $CH< 128)
            return true;

        return false;
    } // end function IsBangla

    public function IsSpace($C)
    {
        if( $C==' ' ||  $C=='\t' || $C=='\n'
            ||  $C=='\r')
            return true;

        return false;
    } // end function IsSpace

   public function MapKarToSorborno($CUni)
    {
        $CSorborno = $CUni;
        if($CUni=='া')
            $CSorborno = 'আ';
        else if($CUni=='ি')
            $CSorborno = 'ই';
        else if($CUni=='ী')
            $CSorborno = 'ঈ';
        else if($CUni=='ু')
            $CSorborno = 'উ';
        else if($CUni=='ূ')
            $CSorborno = 'ঊ';
        else if($CUni=='ৃ')
            $CSorborno = 'ঋ';
        else if($CUni=='ে')
            $CSorborno = 'এ';
        else if($CUni=='ৈ')
            $CSorborno = 'ঐ';
        else if($CUni=='ো')
            $CSorborno = 'ও';
        else if($CUni=="ো")
            $CSorborno = 'ও';
        else if($CUni=='ৌ')
            $CSorborno = 'ঔ';
        else if($CUni=="ৌ")
            $CSorborno = 'ঔ';

        return $CSorborno;
    } // end function MapKarToSorborno

    public function MapSorbornoToKar($CUni)
    {
        $CKar = $CUni;
        if($CUni=='আ')
            $CKar = 'া';
        else if($CUni=='ই')
            $CKar = 'ি';
        else if($CUni=='ঈ')
            $CKar = 'ী';
        else if($CUni=='উ')
            $CKar = 'ু';
        else if($CUni=='ঊ')
            $CKar = 'ূ';
        else if($CUni=='ঋ')
            $CKar = 'ৃ';
        else if($CUni=='এ')
            $CKar = 'ে';
        else if($CUni=='ঐ')
            $CKar = 'ৈ';
        else if($CUni=='ও')
            $CKar = 'ো';
        else if($CUni=='ঔ')
            $CKar = 'ৌ';

        return $CKar;
    } // end function MapSorbornoToKar


    public function ReArrangeUnicodeConvertedText($str)
    {

        for ($i=0; $i<$this->customlen($str); $i++)
        {

            // for 'Vowel + HALANT + Consonant'
            // it should be 'HALANT + Consonant + Vowel'
            if ($i > 0 && $this->customcharAt($str,$i) == '\u09CD' && ($this->IsBanglaKar($this->customcharAt($str,$i-1)) || $this->IsBanglaNukta($this->customcharAt($str,$i-1))) && $i < $this->customlen($str)-1)
            {
                $temp = $this->customsubstr($str,0, $i-1);
                $temp .= $this->customcharAt($str,$i);
                $temp .=$this->customcharAt($str,$i+1);
                $temp .=$this->customcharAt($str,$i-1);
                $temp .= $this->customsubstr($str,$i + 2, $this->customlen($str));
                $str = $temp;
            }

            // for 'RA (\u09B0) + HALANT + Vowel'
            // it should be 'Vowel + RA (\u09B0) + HALANT'
            if ($i > 0 && $i < $this->customlen($str) - 1 && $this->customcharAt($str,$i) == '\u09CD' && $this->customcharAt($str,$i-1) == '\u09B0'
                && $this->customcharAt($str,$i-2)!= '\u09CD' && $this->IsBanglaKar($this->customcharAt($str,$i+1)))
            {
                $temp = $this->customsubstr($str,0, $i-1);
                $temp .= $this->customcharAt($str,$i+1);
                $temp .= $this->customcharAt($str,$i-1);
                $temp .= $this->customcharAt($str,$i);
                $temp .= $this->customsubstr($str,$i + 2, $this->customlen($str));
                $str = $temp;
            }

            // Change refs
            if ($i < $this->customlen($str) - 1 && $this->customcharAt($str,$i)=='র' && $this->IsBanglaHalant($this->customcharAt($str,$i+1)) && !$this->IsBanglaHalant($this->customcharAt($str,$i-1))  )
            {
                $j = 1;
                while(true)
                {
                    if($i-$j<0)
                        break;
                    if($this->IsBanglaBanjonborno($this->customcharAt($str,$i-$j)) && $this->IsBanglaHalant($this->customcharAt($str,$i-$j-1)))
                        $j += 2;
                    else if($j==1 && $this->IsBanglaKar($this->customcharAt($str,$i-$j)))
                        $j++;
                    else
                        break;
                }
                $temp = $this->customsubstr($str,0, $i-$j);

                $temp .= $this->customcharAt($str,$i);

                $temp .=$this->customcharAt($str,$i+1);
                $temp .= $this->customsubstr($str,$i-$j, $j);

                $temp .= $this->customsubstr($str,$i+2, $this->customlen($str));

                $str = $temp;

                $i += 1;

            }

            // Change pre-kar to post format suitable for unicode
            if ($i < $this->customlen($str) - 1 && $this->IsBanglaPreKar($this->customcharAt($str,$i)) && $this->IsSpace($this->customcharAt($str,$i+1))==false)
            {

                $temp = $this->customsubstr($str,0, $i);
                $j = 1;
                while($this->IsBanglaBanjonborno($this->customcharAt($str,$i+$j)))
                {
                    if($this->IsBanglaHalant($this->customcharAt($str,$i+$j+1)))
                        $j += 2;
                    else
                        break;
                }

                $temp .= $this->customsubstr($str,$i+1,$j);

                $l = 0;

                if($this->customcharAt($str,$i)=='ে' && $this->customcharAt($str,$i+$j+1) == 'া')
                { $temp .= "ো"; $l = 1;}
                else if($this->customcharAt($str,$i)=='ে' && $this->customcharAt($str,$i+$j+1) == "ৗ")
                { $temp .= "ৌ"; $l = 1;}
                else{
                    $temp .= $this->customcharAt($str,$i);
                }

                $temp .= $this->customsubstr($str,$i+$j+$l+1, $this->customlen($str));
                $str = $temp;
                $i += $j;
            }

            // nukta should be placed after kars
            // if(i<str.length-1 && IsBanglaNukta(str.charAt(i)) && IsBanglaPostKar(str.charAt(i+1)))
            if($i<$this->customlen($str)-1 && $this->customcharAt($str,$i)=='ঁ' && $this->IsBanglaPostKar($this->customcharAt($str,$i+1)))
            {

                $temp = $this->customsubstr($str,0, $i);
                $temp .= $this->customcharAt($str,$i+1);
                $temp .= $this->customcharAt($str,$i);
                $temp .= $this->customsubstr($str,$i+2,$this->customlen($str));
                $str = $temp;
            }
        }

        return $str;
    }

    public function ConvertToUnicode($ConvertFrom, $line)
    {
        $conversion_map = $this->bijoy_string_conversion_map;
        if($ConvertFrom=="bijoy")
            $conversion_map = $this->bijoy_string_conversion_map;

        foreach($conversion_map as $key =>$value)
        {
            $rejax="/$key/";
            //echo $rejax;exit();
            $line = preg_replace($rejax, $value, $line);
            //	var myRegExp = new RegExp($ascii, "g");
            //	$line = line.replace(myRegExp, $conversion_map[$ascii]);
        }
        $line = $this->ReArrangeUnicodeConvertedText ($line);
        $rejax="/অা/";
        //var myRegExp = new RegExp("অা", "g");
        preg_replace($rejax, "আ", $line);
        //$line = line.replace(myRegExp, "আ");

        return $line;
    } // end function ConvertBijoyToUnicode


    public function recheckString($string){
        $temp = $this->customsubstr($string,$this->customlen($string)-1, 1);
        if($this->IsBanglaHalant($temp)){
          return  mb_substr($string,0,$this->customlen($string)-1);
        }
        return $string;
    }

}