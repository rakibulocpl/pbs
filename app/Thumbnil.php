<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thumbnil extends Model
{
    //
    protected $table ='thumbnil';
    protected $fillable =  [
        'epaper_id',
        'epaper_date',
        'page_id',
        'pageno',
        'image_path',
        'image_width',
        'cordinate_font',
        'cordinate_original'
    ];
}
