<?php

namespace App\Http\Controllers;

use App\OfficeLIst;
use App\PbsList;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class CustomLoginController extends Controller
{
    //

    public function showLoginForm()
    {
        $pbslist =PbsList::where('REC_STATUS','A')->pluck('pbs_descr','pbs_code')->toArray();
        return view('auth.login',compact('pbslist'));
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $password = $request->get('password');
        if(strlen($password)!=7)
        {
            Session::flash('error', 'Invalid Mobile number or password!!');
            return redirect()->back()->withInput();
        }
        $mobile = $request->get('mobile');
        $office = $request->get('office_id');
        $pbs = $request->get('pbs_id');
        $book = substr($password,0,3);
        $account = substr($password,3,7);
//        dd('book '.$book.' account '.$account.' password '.$password.' mobile '.$mobile.' office_id '.$office.' pbs_id '.$pbs);


        $userdata = User::where('phone',$mobile)
            ->where('book',$book)
            ->where('account',$account)
            ->where('office_code',$office)
            ->where('pbs_code',$pbs)
            ->first();
        if (empty($userdata)){
            Session::flash('error', 'Invalid Mobile number or password!!');
            return redirect()->back()->withInput();
        }

        $loggedin = Auth::loginUsingId($userdata->id);

        if(!$loggedin)
        {
            Session::flash('error', 'Invalid Mobile number or password!!');
            return redirect()->back()->withInput();
        }
        $officename = OfficeLIst::where('office_code', Auth::user()->office_code)->value('DESCR');
        Session::put('officename',$officename);
        return redirect()->route('home');
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            'office_id' => 'required|string',
            'pbs_id' => 'required|string',
            'mobile' => 'required|string',
            'password' => 'required',
        ]);
    }

    public function getOfficelistBypbsid(Request $request) {
        $pbscode = $request->get('pbscode');

        $officelist = OfficeLIst::where('DIV_CODE', $pbscode)->orderBy('DESCR', 'ASC')->pluck('DESCR', 'OFFICE_CODE')->toArray();
        $data = ['responseCode' => 1, 'data' => $officelist];
        return response()->json($data);
    }
}
