<?php

namespace App\Http\Controllers;

use App\Configuration;
use App\Epaper;
use App\EpaperPages;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Thumbnil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;

class EpaperController extends Controller
{
    public function  index(){
        return view('epaper.list');
    }

    public function getEpaperList(){
//        DB::statement(DB::raw('set @rownum=0'));
        $epaper = Epaper::orderBy('id','desc')->get();
        return Datatables::of($epaper)
            ->addColumn('action', function ($epaper) {
                $pages =  EpaperPages::where('epaper_id',$epaper->id)->count();
                if($epaper->no_of_pages !=$pages){
                    return '<a href="'.route('epaper.uploadimage',[Encryption::encodeId($epaper->id)]).'" class="btn  btn-primary btn-sm"><i class="fa fa-edit"></i> Upload Image</a>';
                }else{
                    return '<a href="'.route('epaper.editpage',[Encryption::encodeId($epaper->id),Encryption::encodeId(1)]).'" class="btn  btn-inverse-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                    <a href="'.route('change.view',[Encryption::encodeId($epaper->id)]).'" class="btn  btn-inverse-info btn-sm"><i class="fa fa-edit"></i> Change Image</a>';
                }
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    public function addnew(){
        return view('epaper.addnew');
    }

    public function store(Request $request){
        $data =[ "epaper_date" => CommonFunction::changeDateFormat($request->get('epaper_date'),true)];
        $aa =Validator::make($data, [
            'epaper_date' => 'required|unique:epaper_master'
        ])->validate();
        $no_of_page= Configuration::where('caption','no_of_page')->value('value') ;
        $epaper = new Epaper();
        $epaper->epaper_date =  CommonFunction::changeDateFormat($request->get('epaper_date'),true);
        $epaper->no_of_pages = $no_of_page;
        $epaper->status = $request->get('status');
        $epaper->save();
        return redirect()->route('epaper.list');
    }

    public function pagelist($epaper_id){
        $epaper_id = Encryption::decodeId($epaper_id);
        $pages = EpaperPages::where('epaper_id',$epaper_id)->get();
        return view('epaper.pagelist',compact('pages'));

    }

    public function uploadimage($epaper_id){
        $epaper_id = Encryption::decodeId($epaper_id);
        $epaperInfo = Epaper::where('id',$epaper_id)->first();
        $pageInfo   = EpaperPages::where('epaper_id',$epaper_id)->get();
        return view('epaper.uploadimage',compact('epaperInfo','pageInfo'));
    }

    public function editpage($epapre_id,$pageno){
        $epapre_id = Encryption::decodeId($epapre_id);
        $pageno = Encryption::decodeId($pageno);
        $pageinfo = EpaperPages::where('epaper_id',$epapre_id)->where('page_no',$pageno)->first();
        $thumbnils =  Thumbnil::where('page_id',$pageinfo->id)->pluck('cordinate_font')->toArray();
        $thumbimages =  Thumbnil::where('page_id',$pageinfo->id)->get(['id','page_id','image_path','news_type','related_image_id']);
//        dd($thumbimages->toArray());
        $no_of_page =  Epaper::where('id',$pageinfo->epaper_id)->value('no_of_pages');
        $thumbnilsarray = [];
        $area_string = '';
        if (count($thumbnils)>0){
            foreach ($thumbnils as $value){
                $keyarrays =  array('x','y','width','height');
                $valueasarray =  explode(',',$value);
                $cordinates = array(
                    floatval($valueasarray[0]),
                    floatval($valueasarray[1]),
                    floatval($valueasarray[2]),
                    floatval($valueasarray[3])
                ) ;
                array_push($thumbnilsarray,array_combine($keyarrays,$cordinates));
            }
           $area_string  = json_encode($thumbnilsarray);
        }
        return view('epaper.editpage',compact('pageinfo','area_string','thumbimages','no_of_page'));
    }

    public function uploadstore($epaper_id,Request $request){
        $epaper_id = Encryption::decodeId($epaper_id);
        $epaperInfo = Epaper::where('id',$epaper_id)->first();
        foreach ($request->image as $key => $value) {
            $page = EpaperPages::firstOrNew(['epaper_date' =>$epaperInfo->epaper_date,'page_no'=>$request->get('page')[$key]]);
            $page->page_no = $request->get('page')[$key];
            $prefix = date('Y_');
            if(isset($request->file('image')[$key])) {
                $page_image = $request->file('image')[$key];
                $path = "uploads/" . date("Y",strtotime($epaperInfo->epaper_date)) . "/" . date("m",strtotime($epaperInfo->epaper_date))."/".date('d',strtotime($epaperInfo->epaper_date))."/pages";
                $path_original = "uploads/" . date("Y",strtotime($epaperInfo->epaper_date)) . "/" . date("m",strtotime($epaperInfo->epaper_date))."/".date('d',strtotime($epaperInfo->epaper_date)).'/original';

                if ($request->hasFile('image')) {
                    $img_file = trim(sprintf("%s", uniqid($prefix, true))) . $page_image->getClientOriginalName();
                    $mime_type = $page_image->getClientMimeType();
                    if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                            $myfile = fopen($path . "/index.html", "w");
                            fclose($myfile);
                        }

                        if (!file_exists($path_original)) {
                            mkdir($path_original, 0777, true);
                            $myfile = fopen($path_original . "/index.html", "w");
                            fclose($myfile);
                        }
                        Image::make( $value )->resize( 2000, 3000)->save( $path_original.'/'.$img_file,100);
                        Image::make( $value )->resize( 780, 1234)->save( $path.'/'.$img_file,80);
                        $page->url = $path . '/' . $img_file;
                        $page->url_original = $path_original . '/' . $img_file;
                        $page->epaper_id = $epaperInfo->id;
                        $page->epaper_date = $epaperInfo->epaper_date;
                        $page->save();
                    } else {
                        \Session::flash('error', 'Page image type must be in png or jpg or jpeg format');
                        return redirect()->back();
                    }
                }else{
                    \Session::flash('error', 'Something Wrong please Contact with admin [error-101]');
                    return redirect()->back();
                }

            }else{
                \Session::flash('error', 'Something Wrong please Contact with admin [error-102]');
                return redirect()->back();
            }
        }
        return redirect()->route('epaper.pagelist',[Encryption::encodeId($epaper_id)]);
    }

    public function changeStore($epaper_id,Request $request){
        $epaper_id = Encryption::decodeId($epaper_id);
        $epaperInfo = Epaper::where('id',$epaper_id)->first();
        DB::beginTransaction();
        if(isset($request->image)){
            foreach ($request->image as $key => $value) {
                $page = EpaperPages::firstOrNew(['epaper_date' =>$epaperInfo->epaper_date,'page_no'=>$request->get('page')[$key]]);
                Thumbnil::where('page_id',$page->id)->delete();
                $page->page_no = $request->get('page')[$key];
                $prefix = date('Y_');
                if(isset($request->file('image')[$key])) {
                    $page_image = $request->file('image')[$key];
                    $path = "uploads/" . date("Y",strtotime($epaperInfo->epaper_date)) . "/" . date("m",strtotime($epaperInfo->epaper_date))."/".date('d',strtotime($epaperInfo->epaper_date))."/pages";
                    $path_original = "uploads/" . date("Y",strtotime($epaperInfo->epaper_date)) . "/" . date("m",strtotime($epaperInfo->epaper_date))."/".date('d',strtotime($epaperInfo->epaper_date)).'/original';

                    if ($request->hasFile('image')) {
                        $img_file = trim(sprintf("%s", uniqid($prefix, true))) . $page_image->getClientOriginalName();
                        $mime_type = $page_image->getClientMimeType();
                        if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
                            if (!file_exists($path)) {
                                mkdir($path, 0777, true);
                                $myfile = fopen($path . "/index.html", "w");
                                fclose($myfile);
                            }

                            if (!file_exists($path_original)) {
                                mkdir($path_original, 0777, true);
                                $myfile = fopen($path_original . "/index.html", "w");
                                fclose($myfile);
                            }
                            Image::make( $value )->resize( 2000, 3000)->save( $path_original.'/'.$img_file,100);
                            Image::make( $value )->resize( 780, 1234)->save( $path.'/'.$img_file,80);
                            $page->url = $path . '/' . $img_file;
                            $page->url_original = $path_original . '/' . $img_file;
                            $page->epaper_id = $epaperInfo->id;
                            $page->epaper_date = $epaperInfo->epaper_date;
                            $page->save();
                        } else {
                            \Session::flash('error', 'Page image type must be in png or jpg or jpeg format');
                            return redirect()->back();
                        }
                    }else{
                        \Session::flash('error', 'Something Wrong please Contact with admin [error-101]');
                        return redirect()->back();
                    }

                }else{
                    \Session::flash('error', 'Something Wrong please Contact with admin [error-102]');
                    return redirect()->back();
                }
            }
        }else{
            \Session::flash('error', 'No page Updated !!');
        }
        DB::commit();
        return redirect()->route('epaper.pagelist',[Encryption::encodeId($epaper_id)]);
    }

    public function storethumbnail(Request $request){
        $pageid = Encryption::decodeId($request->get('pageid'));
        $pageinfo =  EpaperPages::find($pageid);
        $epaper_date = $pageinfo->epaper_date;
        $path = "uploads/" . date("Y",strtotime($pageinfo->epaper_date)) . "/" . date("m",strtotime($pageinfo->epaper_date))."/".date('d',strtotime($pageinfo->epaper_date))."/thumbnail";
        $cropimage_path =  public_path($pageinfo->url_original);

        $croppath = public_path($path);
        if (!file_exists($croppath)) {
            mkdir($croppath, 0777, true);
            $myfile = fopen($croppath . "/index.html", "w");
            fclose($myfile);
        }

        $data = json_decode($request->get('arr'));
        $prefix = 'thumb_';
        $allthumbnil = [];
        DB::beginTransaction();
        foreach ($data as $value) {
            $cordinate_original = $value[0].','.$value[1].','.$value[2].','.$value[3];
            $cordinate_font = $value[0].','.$value[1].','.($value[0]+$value[2]).','.($value[1]+$value[3]);
            $checkexists = Thumbnil::where('cordinate_original',$cordinate_font)->first();
            if($checkexists){
                $allthumbnil[] =  $checkexists->id;;
            }else{
                $img = Image::make($cropimage_path);
                $img_file = trim(sprintf("%s", uniqid($prefix, true))) . '.jpg';
                $x1=ceil($value[0]*2.564);
                $y1=ceil($value[1]*2.431);
                $width=ceil($value[2]*2.564);
                $height=ceil($value[3]*2.431);

                $img->crop($width, $height, $x1, $y1);
                $img->save($croppath.'/'.$img_file);
                $img->destroy();
                $insertdata = array('epaper_id'=>$pageinfo->epaper_id,'pageno'=>$pageinfo->page_no,'epaper_date'=>$epaper_date,'page_id'=>$pageid,'image_path'=>$path.'/'.$img_file,'cordinate_font'=>$cordinate_original,'cordinate_original'=>$cordinate_font,'image_width'=>$width);
                $savedata =  Thumbnil::create($insertdata);
                $allthumbnil[] = $savedata->id;
            }

        }

        if (!empty($allthumbnil)){

            $abc = Thumbnil::where('page_id', $pageinfo->id)->whereNotIn('id', $allthumbnil);

            $thumbnaildelete =  $abc->pluck('id')->toArray();
//            print_r($thumbnaildelete);
//            print_r($allthumbnil);
//            exit();
            $abc->delete();
            $relatedthumb = Thumbnil::whereIn('related_image_id',$thumbnaildelete)
                ->update([
                    'related_image_id'=>NULL,
                    'news_type'=>0
                ]);
        }

            DB::commit();
        return response()->json(['responsecode'=>1]);
        // will echo the JSON.stringified - string:
        // will echo the json_decode'd object

        //traversing the whole object and accessing properties:
        /* foreach($data as $cityObject){
             echo "City: " . $cityObject->city . ", Age: " . $cityObject->age . "<br/>";
        }*/
    }

    public  function  getthumbnilbypageno(Request $request){
        $thumnilbypage = EpaperPages::where('epaper_pages.page_no',$request->get('pageno'))
            ->where('epaper_pages.epaper_id',$request->get('epaperid'))
            ->where('thumbnil.news_type',0)
            ->rightjoin('thumbnil','thumbnil.page_id','epaper_pages.id')
            ->get(['thumbnil.*']);

//        dd($thumnilbypage->toArray());
        $html ="";
        $rowCount = 0;
        $numOfCols = 4;
        $routename =  'epaper.storerelateion';
        if (count($thumnilbypage->toArray())>0){
            $html .= '<form method="post" action="'.route($routename).'">
            <input type="hidden" name="_token" value="'.csrf_token().'"/>
            <input type="hidden" name="parent_image_id" value="'.$request->get('parentid').'"/>
        <div class="col-lg-12">
        	<h3>Please select Image for next page:</h3>';
            $html .= '<div class="row">';
            foreach ($thumnilbypage as $value){
                $html .= '<div class="col-md-3 nopad text-center ">
                        <label class="image-radio">
                            <img class="img-responsive" src="'.asset($value->image_path).'" style="object-fit: contain;height: 300px;width: 300px;">
                            <input type="radio" name="image_radio" value="'.$value->id.'">
                             <i class="glyphicon fa fa-check hidden"></i>
                        </label>
                        </div>';

                $rowCount++;
                if($rowCount % $numOfCols == 0) {
                    $html .='</div><div class="row">';
                }
            }

            $html .='</div></div>
                        <div class="col-lg-12 text-right">
                        <input type="submit" name="radioSubmit" class="btn btn-success" value="SUBMIT">
                        </div>
                        </form>
   	                    </div>';
        }else{
            $html ='<br><br><div class="col-md-12 text-center text-info"><b>No data found</b></div>';
        }
        return response()->json(['responsecode'=>1,'data'=>$html]);
    }

    public function storerelateion(Request $request){
       $parentimage  = Thumbnil::find($request->get('parent_image_id'));
       $nextimage =  Thumbnil::find($request->get('image_radio'));
        $parentimage->news_type  = 1;
        $parentimage->related_image_id  = $request->get('image_radio');
        $parentimage->save();
        $nextimage->news_type  = 2;
        $nextimage->related_image_id  = $request->get('parent_image_id');
        $nextimage->save();
        return redirect()->back();

    }

    /*change image*/

    public function changeimage($epaper_id){
        $epaper_id = Encryption::decodeId($epaper_id);
        $epaperInfo = Epaper::where('id',$epaper_id)->first();
        $pageInfo   = EpaperPages::where('epaper_id',$epaper_id)->get();

        return view('epaper.changepage',compact('epaperInfo','pageInfo'));
    }

    public function getrelatedimage(Request $request){

        $relatedimage =  Thumbnil::where('id',$request->relatedid)->value('image_path');
        if ($relatedimage ==null && $relatedimage ==''){
            return response()->json(['responsecode'=>0]);
        }else{
            return response()->json(['responsecode'=>1,'image_path'=>$relatedimage]);
        }

    }

}
