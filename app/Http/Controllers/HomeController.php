<?php

namespace App\Http\Controllers;

use App\Epaper;
use App\EpaperPages;
use App\Thumbnil;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $latest_epaper = Epaper::orderBy('id','desc')->first();
        $pageno=1;
        $paperdate=$latest_epaper->epaper_date;

        $pageImage = EpaperPages::where('epaper_date',$paperdate)->where('page_no',$pageno)->value('url');
        $noofpage=$latest_epaper->no_of_pages;
        $data['cordinatedata']=$this->showcordinate($pageno,$paperdate);
        $data['paperinfo']=array('paperdate'=>$paperdate,
            'pageno'=>$pageno,
            'noofpage'=>$noofpage,
            'pageimage'=>$pageImage);

        return view('home',compact('data'));
    }

    public function getPageBydatePageno(Request $request){
        $pageno=$request->page;
        $paperdate=$request->date;
        $epaper_by_date = Epaper::where('epaper_date',$paperdate)->first();
        if (empty($epaper_by_date)){
            return view('pageerror');
        }
        $pageImage = EpaperPages::where('epaper_id',$epaper_by_date->id)->where('page_no',$pageno)->value('url');
        if (empty($pageImage)){
            return view('pageerror');
        }
        $data['cordinatedata']=$this->showcordinate($pageno,$paperdate);
        $noofpage=$epaper_by_date->no_of_pages;

        $data['paperinfo']=array('paperdate'=>$paperdate,
            'pageno'=>$pageno,
            'noofpage'=>$noofpage,
            'pageimage'=>$pageImage);

        return view('home',compact('data'));
    }

    public function showcordinate($pageno,$paperdate){
      return  Thumbnil::leftjoin('thumbnil as relatedimage','relatedimage.related_image_id','thumbnil.id')
            ->where('thumbnil.epaper_date',$paperdate)
            ->where('thumbnil.pageno',$pageno)
            ->get([
                'thumbnil.*',
                'relatedimage.image_path as related_path'
            ]);

    }
}
