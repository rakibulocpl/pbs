<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epaper extends Model
{
    protected $table = 'epaper_master';
    protected $fillable =['epaper_date','no_of_pages'];
}
