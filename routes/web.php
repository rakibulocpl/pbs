<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Auth::routes();
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/epaper', 'HomeController@getPageBydatePageno') ;

    Route::middleware(['auth'])->group(function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('/epaper/list', 'EpaperController@index')->name('epaper.list');
        Route::get('/epaper/pagelist/{epaper_id}', 'EpaperController@pagelist')->name('epaper.pagelist');
        Route::get('/epaper/uploadimage/{epaper_id}', 'EpaperController@uploadimage')->name('epaper.uploadimage');
        Route::post('/epaper/uploadimage/{epaper_id}', 'EpaperController@uploadstore');
        Route::get('/epaper/changeimage/{epaper_id}', 'EpaperController@changeimage')->name('change.view');
        Route::post('/epaper/changeimage/{epaper_id}', 'EpaperController@changeStore')->name('change.store');

//    edit page
        Route::get('/epaper/editpage/{epaper_id}/{page_id}', 'EpaperController@editpage')->name('epaper.editpage');
        Route::post('/epaper/storethumbnail', 'EpaperController@storethumbnail')->name('epaper.storethumbnail');
        Route::post('/epaper/get-related-image', 'EpaperController@getrelatedimage')->name('epaper.getrelatedimage');

        Route::get('/epaper/add', 'EpaperController@addnew')->name('epaper.addnew');
        Route::post('/epaper/add', 'EpaperController@store')->name('epaper.store');
        Route::get('/epaper/edit', 'EpaperController@edit')->name('epaper.edit');
        Route::get('/epaper/getleavelist', 'EpaperController@getEpaperList')->name('epaper.getEpaperlist');
        Route::post('/epaper/getthumbnilbypageno', 'EpaperController@getthumbnilbypageno')->name('epaper.getthumbnilbypageno');
        Route::post('/epaper/storerelateion', 'EpaperController@storerelateion')->name('epaper.storerelateion');
        
    });

    /*admin*/




