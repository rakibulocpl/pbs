jQuery(document).ready(function() {

    $('img#example').selectAreas({
                    minSize: [10, 10],
                    onChanged: debugQtyAreas,
                    width:780
                });
    $('#btnView').click(function () {
                    var areas = $('img#example').selectAreas('areas');
                    if (areas.length==0) {
                       $.createAlert({
                      attachAfter: '#btnView',
                      title: 'Select At lest One Area',
                      confirmText: 'OK',
                      confirmStyle: 'blue',
                      callback: null
                    });
                  $.showAlert();
                }else{
              
                    var checkresult=checkminheightwidth(areas);
                    if (checkresult.length==0) {
                      window.alert('Area is Too Small..!!');
                    }else{
                      displayAreas(areas);
                    }
                }
                                
             });

    showthumbil();
     $(function () {
        var wWidth = $(window).width();
        var dWidth = wWidth * 0.8;
        var wHeight = $(window).height();
        var dHeight = wHeight * 0.8;

        $("#mySite").dialog({
            dialogClass: "flora",
            autoOpen: false,
            position:'center',
             width: dWidth,
            modal: true,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
           
        });

    });

  $('.flora.ui-dialog').css({position:"fixed"});
  $(".ui-resizable").stop(function() {
    $(".flora.ui-dialog").css({position:"fixed"});
  });

  $('.prvspointer').on('click',function(){
   
  });



    

});

/* $(document).on('click', '.img', function() { 
    $("#mySite").find('.details').empty();
    var srcvalue=$(this).attr('src');
         $("#mySite").find('.details').append('<img width="200px" height="200px" src="'+srcvalue+'" alt="thumbnil image" />'+
            '<input type="button" name="thumbnildelbtn" image-path="'+srcvalue+'" id="thumbnildelbtn" value="Delete">'+
            '<input type="button" name="add_next_page"  image-path="'+srcvalue+'" value="Add Next Page" id="addnextpage"/>');
        $("#mySite").dialog("open");
  });*/
/*$(document).on('click', '#thumbnildelbtn', function(){
     var path=$(this).attr('image-path');
                $.ajax({
                   type: "POST",
                   url: "http://localhost/epaper_new/upload/delete_thumbnil/",
                   data: {imagepath:path},
                   success: function(data){
                    console.log("success:",data);
                     $("#mySite").dialog("close");
                         window.alert('News Delted Successfully');
                     },
                   failure: function(errMsg) {
                        console.error("error:",errMsg);
                   }
                });
          });*/

$(document).on('click', '.addnextpage', function(){
    var path=$(this).attr('img-path');
    var imgname=$(this).attr('img-name');
    $('.modal-body').empty();
/*    document.getElementById('nextpageimage').src=path;
    document.getElementById('nextpageimage').name=imgname;*/
    $('.modal-body').append('<div id="DivIdToPrint" class="modal-main-img" style="overflow-x: auto;"><center><table style="width: 100%">'+
             '<tr><td width="50%" style="text-align: center;"><img width="300px" height="200px" id="nextpageimage"  src="'+path+'" '+
             'name="'+imgname+'" alt="thumbnil image" /></td><td width="50%" style="text-align: center;"><input type="text" style="float:'+ 
             'left;width: 120px;padding: 8px;" name="inputpageno" id="inputpageno" placeholder="Enter Page No"/>'+
            '<input type="text" style="float: left;width: 120px;padding: 8px;" name="inputcolumnno" id="inputcolumnno"'+
             'placeholder="Column No"/><input type="button" name="nexpagesubmit" value="ADD" class="btn" id="nexpagesubmit"></td>'+
            '</tr> </table></center><div style="margin: 20px;" id="nextpageoutput"></div></div>');
    document.getElementById('newsPopup').style.display="block";
  
});

$(document).on('click', '.nextpointer', function(){
  var relatedimage=$(this).attr('related-image');
   var mainimage=$(this).attr('main-image');
  $('.modal-body').empty();
  $('.modal-body').append("<input related-image='"+relatedimage+"' main-image='"+mainimage+"' type='button' id='delrelated' name='delrelated' value='-' class='btn btn-danger testdel'/><img src='http://localhost/epaper_new/kepaperimage/"+relatedimage+"'/>");
  document.getElementById('newsPopup').style.display="block";
});

$(document).on('click', '.prvspointer', function(){
  var relatedimage=$(this).attr('related-image');
  $('.modal-body').empty();
  $('.modal-body').append("<img src='http://localhost/epaper_new/kepaperimage/"+relatedimage+"'/>");
  document.getElementById('newsPopup').style.display="block";
});

$(document).on('click', '#delrelated', function(){
  var relatedimage=$(this).attr('related-image');
  var mainimage=$(this).attr('main-image');
    $.ajax({
             type: "POST",
             url: "http://localhost/epaper_new/upload/deleterelated/",
             data: {mainimage:mainimage,relatedimage:relatedimage},
             success: function(data){
                      $.createAlert({
                attachAfter: '#delrelated',
                title:'Related news remove successfully!!',
                confirmText: 'OK',
                confirmStyle: 'blue',
                callback: null
              });
          $.showAlert();

        },
             failure: function(errMsg) {
                  console.error("error:",errMsg);
             }
          });
});


$(document).on('click', '.testdel', function(){
     var path=$(this).attr('img-path');
     var r = confirm("Are you sure ?");
      if (r==true) {
        $.ajax({
                   type: "POST",
                   url: "http://localhost/epaper_new/upload/delete_thumbnil/",
                   data: {imagepath:path},
                   success: function(data){
                    console.log("success:",data);
                     $.createAlert({
                      attachAfter: '.testdel',
                      title: 'News Deleted Successfully',
                      confirmText: 'OK',
                      confirmStyle: 'blue',
                      callback: null
                    });
                    $.showAlert();
                     },
                   failure: function(errMsg) {
                        console.error("error:",errMsg);
                   }
                });
      }

});

  $(document).on('click', '#nexpagesubmit', function(){
        var pageno=$('#inputpageno').val();
        var colno=$('#inputcolumnno').val();
        if (pageno!="" || colno!="") {
          $.ajax({
                   type: "POST",
                   url: "http://localhost/epaper_new/upload/addnextpage/",
                   data: {pageno:pageno,columnno:colno},
                   success: function(data){
                       document.getElementById('nextpageoutput').innerHTML=data;
                      },
                   failure: function(errMsg) {
                        console.error("error:",errMsg);
                   }
                });
        }else{
              $.createAlert({
            attachAfter: '#nexpagesubmit',
            title: 'Page No Or Col No Must Not empty',
            confirmText: 'OK',
            confirmStyle: 'blue',
            callback: null
          });
          $.showAlert();
        }
         
        
    });
        
  $(document).on('click', '#saveextpage', function(){
        if (!$('input[name=nextpagechecked]:checked').val()) {
            $.createAlert({
                      attachAfter: '#saveextpage',
                      title: 'Select A News...',
                      confirmText: 'OK',
                      confirmStyle: 'blue',
                      callback: null
                    });
                $.showAlert();

         }else{
           var nextpagename=$('input[name=nextpagechecked]:checked').val();
           var imagename=$('#nextpageimage').attr('name');
           $.ajax({
                   type: "POST",
                   url: "http://localhost/epaper_new/upload/update_nextpage/",
                   data: {childimg:nextpagename,parentimg:imagename},
                   success: function(data){
                  
                   $.createAlert({
                      attachAfter: '#nextpageimage',
                      title: 'Next Page Added Successfully...',
                      confirmText: 'OK',
                      confirmStyle: 'blue',
                      callback: null
                    });
                $.showAlert();
                document.getElementById('newsPopup').style.display="none";
                         },
                   failure: function(errMsg) {
                        console.error("error:",errMsg);
                   }
                });
         }
     
        
    });


jQuery(".pagelist li").click(function () {
        //alert("Clicked on " + this.id);
          document.getElementById("pagenumber").innerHTML = $(this).text();
         // $('#edit-content').empty();
          var imgurl=$('#imageurl').text();
          console.clear(); 
          document.getElementById('example').src=imgurl+'/'+$(this).text()+'.jpg';
          showthumbil();

    });


        var selectionExists;

            function areaToString (area) {
                return (typeof area.id === "undefined" ? "" : (area.id + ": ")) + area.x + ':' + area.y  + ' ' + area.width + 'x' + area.height + '<br />'
            }

            function output (text) {
                $('#output').html(text);
            }

            // Log the quantity of selections
            function debugQtyAreas (event, id, areas) {
                console.log(areas.length + " areas", arguments);
            }

            // Display areas coordinates in a div
            function displayAreas (areas) {
                var text =[];
                $.each(areas, function (id, area) {
                    text[id] =[area.x,area.y,area.width,area.height];
                });
                if (text.length==0) {
                       $.createAlert({
                      attachAfter: '#btnView',
                      title: 'Select At lest One Area',
                      confirmText: 'OK',
                      confirmStyle: 'blue',
                      callback: null
                    });
                $.showAlert();
                }else{
                  var baseurl=$('#imageurl').text();
                var pagenumber=$('#pagenumber').text();
                $.ajax({
                   type: "POST",
                   url: "http://localhost/epaper_new/upload/uploadthamnil/",
                   data: {arr: JSON.stringify(text),urldata: baseurl+pagenumber+'.jpg',pageno:pagenumber},
                   success: function(data){
                            $.createAlert({
                      attachAfter: '#btnView',
                      title: data,
                      confirmText: 'OK',
                      confirmStyle: 'blue',
                      callback: null
                    });
                $.showAlert();
              },
                   failure: function(errMsg) {
                        console.error("error:",errMsg);
                   }
                });
                output(JSON.stringify(text));
                showthumbil();

                }
              
            }

            function checkminheightwidth(areas){
                var text =[];
                $.each(areas, function (id, area) {
                  if (area.width<20 || area.height<20) {
                    text =[];
                    return  false;
                  }else{
                    text[id] =[area.x,area.y,area.width,area.height];
                  }
                });

                return text;
            }

             function showthumbil (areas) {
                var pageno =$('#pagenumber').text();
                $.ajax({
                   type: "POST",
                   url: "http://localhost/epaper_new/upload/show_thumbnil/",
                   data: {pageno:pageno},
                   success: function(data){
                         $('#thumbnil-output2').html(data);},
                   failure: function(errMsg) {
                        console.error("error:",errMsg);
                   }
                });
            }
				
