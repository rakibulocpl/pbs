@extends('layouts.admin')

<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.selectareas.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/pagination.css')}}">
<style>
    .hidden {
        display: none !important;
        visibility: hidden !important;
    }
    .custom-thumb{
        margin: 5px;
        border: 1px solid purple;
        padding: 5px;
        border-radius: 5px;
    }
    .custom-thumb img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 300px;
        min-height: 300px;
        max-height: 200px;
        object-fit:contain;
    }

    .custom-thumb img:hover {
        box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
    }
    .nopad {
        padding-left: 0 !important;
        padding-right: 0 !important;
    }

    /* image radio list */
    .image-radio {
        cursor: pointer;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        border: 4px solid transparent;
        margin-bottom: 0;
        outline: 0;
    }
    .image-radio input[type="radio"] {
        display: none;
    }
    .image-radio-checked {
        border-color: #4783B0;
    }
    .image-radio .glyphicon {
        position: absolute;
        color: #4A79A3;
        background-color: #fff;
        padding: 10px;
        top: 0;
        right: 0;
    }
    .image-radio-checked .glyphicon {
        display: block !important;
        visibility: visible !important;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-10 text-left">
                            <div class="pagination">
                                <?php
                                $paperdate=$pageinfo->epaper_date;
                                $noofpage=$no_of_page;
                                $pageno=$pageinfo->page_no;
                                ?>
                                <ul>
                                    @if($pageno !=1)
                                        <a href="<?php echo "/epaper/editpage/".\App\Libraries\Encryption::encodeId($pageinfo->epaper_id)."/".\App\Libraries\Encryption::encodeId($pageinfo->page_no-1) ?>"><i class="fa fa-angle-double-left"></i></a>
                                    @else
                                        <i class="fa fa-angle-double-left"></i>
                                    @endif
                                    <?php
                                    for ($i=1; $i<=$no_of_page; $i++) {
                                    ?>
                                    <li><a {{$pageno ==$i? 'class=active':''}} href="<?php echo "/epaper/editpage/".\App\Libraries\Encryption::encodeId($pageinfo->epaper_id)."/".\App\Libraries\Encryption::encodeId($i) ?>"><?php echo $i;?></a></li>
                                    <?php
                                    }
                                    ?>

                                    @if($pageno !=$no_of_page)
                                        <a href="<?php echo "/epaper/editpage/".\App\Libraries\Encryption::encodeId($pageinfo->epaper_id)."/".\App\Libraries\Encryption::encodeId($pageinfo->page_no+1) ?>"><i class="fa fa-angle-double-right"></i></a>
                                    @else
                                        <i class="fa fa-angle-double-right"></i>
                                    @endif

                                </ul>
                            </div>
                        </div>
                        <div class="col-2 text-right">
                            <button class="btn btn-info" id="savebtn"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <div style="position: relative; width: 780px; height: 1234px;">
                                <img id="example" src="{{asset($pageinfo->url_original)}}" class="blurred" style="width: 780px;height: 1234px; position: absolute;">
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row" id="thumbnils">
                        @if(count($thumbimages)>0)
                            <h4>Thumbnils list</h4>
                            <hr>
                            @foreach($thumbimages as $value)
                                <div class="col-md-12 custom-thumb">
                                    <div class="col-md-4 float-left" data-previous ="{{$value->image_path}}">
                                        <img src="{{asset($value->image_path)}}" class="img-fluid img-thumbnail" alt="thumb image"/>
                                    </div>
                                    <div class="col-md-4 float-right">
                                            @if($value->news_type == 0)
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="addrelatedimage('{{$value->id}}','{{$value->image_path}}')">Click for Add Next<i class="mdi mdi-play-circle ml-1"></i></button>
                                            @else
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="viewlatedimage('{{$value->related_image_id}}')"> View Next/Previous page<i class="mdi mdi-play-circle ml-1"></i></button>
                                            @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Next Page</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script src="{{asset('js/jquery.selectareas.min.js')}}"></script>
    <script>
        var imageselected =  '{!! $area_string !!}';
        if(imageselected !=''){
            var imageselected2 =  JSON.parse(imageselected);
        }else {
            var imageselected2 = [];
        }

        $('img#example').selectAreas({
            minSize: [10, 10],
            width:780,
            areas: imageselected2
        });

        $('#savebtn').click(function () {
            var areas = $('img#example').selectAreas('areas');
            if (areas.length==0) {
                swal({
                    text: 'Select At lest One Area',
                    icon: "error",
                    button: "Ok",
                });
            }else{

                var checkresult=checkminheightwidth(areas);
                if (checkresult.length==0) {
                    window.alert('Area is Too Small..!!');
                }else{
                    displayAreas(areas);
                }
            }

        });

        function checkminheightwidth(areas){
            var text =[];
            $.each(areas, function (id, area) {
                if (area.width<20 || area.height<20) {
                    text =[];
                    return  false;
                }else{
                    text[id] =[area.x,area.y,area.width,area.height];
                }
            });

            return text;
        }

        function displayAreas (areas) {

            var text =[];
            $.each(areas, function (id, area) {
                text[id] =[area.x,area.y,area.width,area.height];
            });
            var pageid='{{\App\Libraries\Encryption::encodeId($pageinfo->id)}}';
            $.ajax({
                type: "POST",
                url: "{{route('epaper.storethumbnail')}}",
                data: {arr: JSON.stringify(text),pageid:pageid,_token : '<?php echo csrf_token() ?>'},
                success: function(data){
                    if (data.responsecode ==1){
                        swal({
                            text: 'Image croped successfully',
                            icon: "success",
                            button: "Done",
                        });
                    }
                },
                failure: function(errMsg) {
                    console.error("error:",errMsg);
                }
            });


        }

        function  addrelatedimage(thumbid,thumbpath) {
            var no_of_page = '{{$no_of_page}}';
            var option = '<select class="form-control" id="pagenumber" parentid='+thumbid+' name="page_no"> <option value="">Select page</option>';

            for (var i=1; i<=no_of_page;i++){
                option += '<option value="' + i + '">Page-' + i + '</option>';
            }

            option +='</select>';
            var modalbody = '<div class="row"> <div class="col-md-12 custom-thumb">' +
                '<div class="col-md-4 float-left">  ' +
                '<img src="/'+thumbpath+ '" style="min-height: 200px;max-height: 200px;" class="img-fluid img-thumbnail"  alt="thumb image"/>' +
                '</div>' +
                '<div class="col-md-4 float-left">'+option+'</div>' +
                '</div></div>' +
                '<div class="row" id="thumbnilbypage"></div>';
            $(".modal-body").html(modalbody);
            $('#exampleModalLabel').html('Add next page')
            $("#exampleModal").modal('show');
        }

        function  viewlatedimage(thumbid) {
            $.ajax({
                type: "POST",
                url: "{{route('epaper.getrelatedimage')}}",
                data: {relatedid: thumbid,_token : '<?php echo csrf_token() ?>'},
                success: function(response){
                    if (response.responsecode ==1){
                        var modalbody = '<img src="/'+response.image_path+'"/>';
                        $(".modal-body").html(modalbody);
                        $('#exampleModalLabel').html('Related Image view');
                        $("#exampleModal").modal('show');
                    }else{
                        alert('NO data found');
                        location.reload();
                    }
                },
                failure: function(errMsg) {
                    console.error("error:",errMsg);
                }
            });
        }


        $(document).on('change','#pagenumber',function (e) {
            var epaperid =  '{{$pageinfo->epaper_id}}';
             var pageno  = e.target.value;
             var parentid =  $(this).attr('parentid');
            $.ajax({
                type: "POST",
                url: "{{route('epaper.getthumbnilbypageno')}}",
                data: {pageno: pageno,epaperid:epaperid,parentid:parentid,_token : '<?php echo csrf_token() ?>'},
                success: function(response){
                    if (response.responsecode ==1){
                      var placethumb =  document.getElementById('thumbnilbypage');
                      $('#thumbnilbypage').html(response.data);
                    }
                },
                failure: function(errMsg) {
                    console.error("error:",errMsg);
                }
            });

        });

        $(document).ready(function(){

            // add/remove checked class
            $(".image-radio").each(function(){
                if($(this).find('input[type="radio"]').first().attr("checked")){
                    $(this).addClass('image-radio-checked');
                }else{
                    $(this).removeClass('image-radio-checked');
                }
            });

            // sync the input state
            $(document).on("click",'.image-radio', function(e){
                $(".image-radio").removeClass('image-radio-checked');
                $(this).addClass('image-radio-checked');
                var $radio = $(this).find('input[type="radio"]');
                $radio.prop("checked",!$radio.prop("checked"));

                e.preventDefault();
            });

        });

    </script>

@endsection
