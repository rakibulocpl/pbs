@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-12">
                            <div class="col-md-6 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Upload Pages</h4>
                                        <form class="forms-sample" action="{{route('epaper.uploadimage',[\App\Libraries\Encryption::encodeId($epaperInfo->id)])}}" method="post" id="epaperform" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Page No</th>
                                                        <th>Image</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @for ($i = 1; $i<=$epaperInfo->no_of_pages; $i++)
{{--                                                        {{dd(2)}}--}}
                                                        <tr>
                                                            <td class="required-star">Page - {{$i}}</td>
                                                            <input type="hidden" name="page[{{$i}}]" value="{{$i}}">
                                                            <td><input type="file" name="image[{{$i}}]" class="required"> </td>
                                                        </tr>
                                                    @endfor
                                                </tbody>
                                            </table>

                                            <button type="submit" class="btn btn-success mr-2">Save</button>
                                            <a class="btn btn-light" href="{{route('epaper.list')}}">Cancel</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script>
        jQuery.validator.messages.required = "";
        $("#epaperform").validate();
        var date = new Date();

        $('.datepicker').datetimepicker({
            viewMode: 'days',
            format: 'DD-MMM-YYYY',
            maxDate:date.setDate(date.getDate() + 2),
            minDate:  (new Date())
        });

    </script>

@endsection
