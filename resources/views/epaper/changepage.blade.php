@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-12">
                            <div class="col-md-8 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Change or Update Pages</h4>
                                        <form class="forms-sample" action="{{route('change.store',[\App\Libraries\Encryption::encodeId($epaperInfo->id)])}}" method="post" id="epaperform" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Page No</th>
                                                    <th>Image</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($pageInfo as $key=>$value)
                                                    <tr>
                                                        <td class="required-star">Page - {{$value->page_no}}</td>
                                                        <input type="hidden" name="page[{{$value->page_no}}]" value="{{$value->page_no}}">
                                                        <td><input type="file" name="image[{{$value->page_no}}]" class="">
                                                            <a href="/{{$value->url}}" target="_blank" class="btn btn-info btn-xs">View image</a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>

                                            <button type="submit" class="btn btn-success mr-2">Save</button>
                                            <a class="btn btn-light" href="{{route('epaper.list')}}">Cancel</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script>
        jQuery.validator.messages.required = "";
        $("#epaperform").validate();
        var date = new Date();

        $('.datepicker').datetimepicker({
            viewMode: 'days',
            format: 'DD-MMM-YYYY',
            maxDate:date.setDate(date.getDate() + 2),
            minDate:  (new Date())
        });

    </script>

@endsection
