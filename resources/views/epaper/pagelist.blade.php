@extends('layouts.admin')
@section('content')
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Epaper List by date</h6>
                    <div class="d-flex table-responsive">
                        <div class="btn-group pull-right">
                            <a href="{{route('epaper.addnew')}}" class="btn btn-sm btn-info"><i class="mdi mdi-plus-circle-outline"></i> Add New</a>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table mt-3 border-top" id="epaperlisttable">
                            <thead>
                            <tr>
                                <th>Page No</th>
                                <th>No of thumbs</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $value)
                                <tr>
                                    <td>
                                        {{$value->page_no}}
                                    </td>
                                    <td>
                                       0
                                    </td>
                                    <td>
                                        <a href="{{route('epaper.editpage',[\App\Libraries\Encryption::encodeId($value->epaper_id),\App\Libraries\Encryption::encodeId($value->page_no)])}}" class="btn btn-info">Edit</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    @include('inc/datatable-script')

@endsection
