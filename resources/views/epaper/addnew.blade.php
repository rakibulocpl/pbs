@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if($errors->all())
                        <div class="alert alert-danger">
                            <ul>@foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>@endforeach</ul>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="col-md-6 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Add New Epaper</h4>
                                        <form class="forms-sample" action="{{route('epaper.store')}}" method="post" id="epaperform">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                            <div class="form-group"  >
                                                <label>Date</label>
                                                <div class="datepicker input-group date {{$errors->has('epaper_date') ? 'has-error': ''}}">
                                                    {!! Form::text('epaper_date','',
                                                    ['class' => 'form-control  input-sm required', 'placeholder' => 'dd-mm-yyyy']) !!}
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                            <div class="form-group"  >
                                                <label>Remarks</label>
                                                {!! Form::textarea('remarks','', $attributes = array('class'=>' form-control','id'=>"comment")) !!}
                                            </div>

                                            <div class="form-group"  >
                                                <label>Status</label>
                                                {!! Form::select('status',[0=>'Pending','1'=>'Approved'],'', $attributes = array('class'=>'js-example-basic-single form-control required',
                           'id'=>"parent_category")) !!}
                                            </div>


                                            <button type="submit" class="btn btn-success mr-2">Save</button>
                                            <a class="btn btn-light" href="{{route('epaper.list')}}">Cancel</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script>
        jQuery.validator.messages.required = "";
        $("#epaperform").validate();
        var date = new Date();

        $('.datepicker').datetimepicker({
            viewMode: 'days',
            format: 'DD-MMM-YYYY',
            maxDate:date.setDate(date.getDate() + 2),
            minDate:  (new Date())
        });

    </script>

@endsection
