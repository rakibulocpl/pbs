@extends('layouts.admin')
@section('content')
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Epaper List by date</h6>
                    <div class="d-flex table-responsive">
                        <div class="btn-group pull-right">
                            <a href="{{route('epaper.addnew')}}" class="btn btn-sm btn-info"><i class="mdi mdi-plus-circle-outline"></i> Add New</a>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table mt-3 border-top" id="epaperlisttable">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>No of pages</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    @include('inc/datatable-script')
    <script>
        $(function () {
            $('#epaperlisttable').DataTable({
                processing: true,
                serverSide: true,
                iDisplayLength: 10,
                ajax: {
                    url: '{{route("epaper.getEpaperlist")}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'epaper_date', name: 'epaper_date'},
                    {data: 'no_of_pages', name: 'no_of_pages'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                "aaSorting": []
            });
        });
    </script>
@endsection
