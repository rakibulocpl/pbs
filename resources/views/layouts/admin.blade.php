<!doctype html>
<html class="no-js" lang="en">
<head>
    <style>
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(/images/Preloader_2.gif) center no-repeat #fff;
        }
        .has-error .form-control {
            border-color: #a94442;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        }
    </style>
    @include('inc/admin-header')
</head>
<body>
<div class="se-pre-con"></div>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('inc/admin/navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <div class="row row-offcanvas row-offcanvas-right">
            <!-- partial:partials/_settings-panel.html -->
            @include('inc/admin/sidebar-right')
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            @include('inc/admin/sidebar')
            <!-- partial -->
            <div class="content-wrapper">
            @include('inc/message')
                @yield('content')
                <!-- partial:partials/_footer.html -->
                @include('inc/admin/footer')
                <!-- partial -->
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- all js here -->
@include('inc/admin-footer')

@yield('footer-script')
<script>
    $(window).on('load',function () {
        $(".se-pre-con").fadeOut("slow");
    }); 
</script>

</body>
</html>
