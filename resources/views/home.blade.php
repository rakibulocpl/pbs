@extends('layouts.epaper')

@section('content')
    <style type="text/css">img {
            display: block;
            margin: 0 auto;
        }
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .btn-danger {
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
        }
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 9999999999; /* Sit on top */
            padding-top: 20px; /* Location of the box */

            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.8); /* Black w/ opacity */
        }
        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;

            /*padding-bottom: 20px;*/

        }

        /* The Close Button */
        .zoomin, .zoomout, .ipt, .close, .share {
            font-size: 11px;
            font-weight: bold;
            border-radius: 0;
            padding: 4px 8px;
            opacity: 0.8;
            background: transparent;

        }
        .close:hover,
        .close:focus {
            background-color: #FFF;
            color:#d9534f;
            border: 1px solid #d9534f;
            text-decoration: none;
            cursor: pointer;

        }
        .modal-head table{ background-color: #2b2a2a; }
        .modal-footer p{
            background-color: #2b2a2a;
            border: 1px solid #2b2a2a;
            color: #e5e5e5;
            padding: 15px 7px;
            font-style: italic;
        }

        .modal-head{}
        .modal-footer{}
        .modal-main-img{padding-left: 5px; padding-right: 5px; padding-top: 0px; padding-bottom: 0px;}

        /*modal customized*/
        .customized_content{

            background-color: #FFF;
            margin-bottom: 50px;

        }

        .modal_table{

        }


        /*loading gif*/
        .loading_img{
            background-image: url('../img/loading.gif');
            background-repeat: no-repeat;
            background-position: center;
        }
        .pull-right {
            float: right!important;
        }
        .pull-left {
            float: left;
        }
        .text-center {
            text-align: center;
        }
    </style>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="wrap">
        <div class="warp-content">
            <div class="very_fast_header">
                <div class="pull_left" style="margin-top:10px;">
                    <a class="top_left" href="">অনলাইন বাংলা  <i class="fas fa-sign-in-alt"></i></a>
                </div>
                <div class="pull_right top_right">
                    <i class="fab fa-facebook facebook_color"></i>
                    <i class="fab fa-twitter-square twitter_color"></i>
                    <i class="fab fa-google-plus-square google_color"></i>
                </div>
            </div>

            <div class="header">

                <div class="second_header">
                    <span>শিরোনাম</span>
                    <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                        <ul class="first_menu">

                            <li style="margin-right:20px;"><i class="fab fa-gg-circle"></i><a href="">বাংলাদেশের উন্নয়নে অংশীদার হতে চাই: সৌদি যুবরাজ</a></li>
                            <li style="margin-right:20px;"><i class="fab fa-gg-circle"></i><a href="">&#039;আওয়ামী লীগের জোটে আসতে চায় অনেক দল&#039;</a></li>
                            <li style="margin-right:20px;"><i class="fab fa-gg-circle"></i><a href="">জেদ্দায় বাংলাদেশ কনস্যুলেট ভবনের ভিত্তিপ্রস্তর স্থাপন করলেন প্রধানমন্ত্রী</a></li>
                            <li style="margin-right:20px;"><i class="fab fa-gg-circle"></i><a href="">আইয়ুব বাচ্চুর জানাজা শুক্রবার, দাফন শনিবার</a></li>
                            <li style="margin-right:20px;"><i class="fab fa-gg-circle"></i><a href="">সংসদ নির্বাচনের তফসিল নভেম্বরের প্রথম সপ্তাহে</a></li>
                        </ul>
                    </marquee>

                </div>
                <div class="third_header">

                    <ul class="second_menu">
                        <li class="dropdown"><a href="">আজকের পত্রিকা <i class="fas fa-sort-down"></i></a>
                            <ul class="dropdown-content">
                                <li><a href="">খবর</a></li>
                                <li><a href="">লোকালয়</a></li>
                                <li><a href="">সম্পাদকীয় ও মন্তব্য</a></li>
                                <li><a href="">শারদীয় দুর্গোৎসব</a></li>
                                <li><a href="">শিল্প বাণিজ্য</a></li>
                                <li><a href="">আন্তর্জাতিক</a></li>
                                <li><a href="">পাঠশালা</a></li>
                                <li><a href="">খেলাধুলা</a></li>
                                <li><a href="">ভোট আসছে</a></li>
                                <li><a href="">শেষের পাতা</a></li>
                                <li><a href="">নন্দন</a></li>
                            </ul>
                        </li>
                        <li><a href="">সকল পাতা</a></li>
                        <li><a href="">{{\App\Libraries\CommonFunction::convertToBanglaDate(date('D-d-m-Y'))}}</a></li>
                        <li><a href="">নিউজ সংস্করণ</a></li>
                        <li><a href="">প্রিন্ট</a></li>
                    </ul>

                </div>
            </div>
            <div class="first_section">
                <div class="pull_left clear">
                    <div class="pagination">
                        <?php
                        $paperdate=$data['paperinfo']['paperdate'];
                        $noofpage=$data['paperinfo']['noofpage'];
                        $pageno=$data['paperinfo']['pageno'];

                        $imagelink=$data['paperinfo']['pageimage'];
                        $noofpage=2;
                        ?>
                        <ul>
                            @if($pageno !=1)
                                <a href="<?php echo "/epaper?date=".$paperdate."&page=".($pageno-1).""; ?>"><i class="fas fa-angle-double-left"></i></a>
                            @else
                                <i class="fas fa-angle-double-left"></i>
                            @endif
                            <?php
                            for ($i=1; $i<=$noofpage; $i++) {
                            ?>
                            <li><a {{$pageno ==$i? 'class=active':''}} href="<?php echo "/epaper?date=".$paperdate."&page=".$i.""; ?>"><?php echo $i;?></a></li>
                            <?php
                            }
                            ?>

                            @if($pageno !=$noofpage)
                                <a href="<?php echo "/epaper?date=".$paperdate."&page=".($pageno+1).""; ?>"><i class="fas fa-angle-double-right"></i></a>
                            @else
                                <i class="fas fa-angle-double-right"></i>
                            @endif

                        </ul>
                    </div>
                </div>
                <div class="pull_right" style="margin:0;">
                    <div class="puran_shonkha tooltip">
                        <span class="tooltiptext">পুরনো সংখ্যা</span>
                        <div id="datepick" style="height: 27px; padding: 0; margin: 0">
                            <button class="nbtn"> সার্চ </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="second_section">
                <div class="pull_left">
                    <div class="main-img-div">

                        <div  class="map maphilighted">
                            <canvas style="opacity: 1;" height="1240" width="780"></canvas>

                            <img src="/<?php echo $imagelink;?>" usemap="#enewspaper" class="map maphilighted"
                                 style="opacity: 0; position: absolute; left: 0px; top: 0px; padding: 0px; border: 0px none;">
                        </div>

                        <map name="enewspaper">
                            <?php
                            foreach ($data['cordinatedata'] as $value) {
                                $location="'".asset('/')."'";
                                $img="'".$value->image_path."'";
                                $related_image="'".$value->related_path."'";
                                $image_width="'".($value->image_width+50)."'";
                                $newstype="'".($value->news_type)."'";
                                echo '<area shape="rect" coords="'.$value->cordinate_original.'" onclick="showmodel('.$img.','.$related_image.','.$location.','.$image_width.','.$newstype.')" data-image="'.$value->image_name.'" class="main-img">';
                            }?>

                        </map>
                    </div>
                </div>
                <div class="pull_right" style="margin:0;">

                    <div class="widget_wrap">
                        <div class="widget">
                            <div class="wmenu">
                                <a href="#">সর্বশেষ খবর</a>
                                <ul>
                                    <li><a href="">ইউরোপে বাংলাদেশি ইলেকট্রনিক্স পণ্যের বাজার সম্প্রসারণ টার্গেট</a></li>
                                    <li><a href="">মৃত্যুর ছয় দিন আগে নিজের কবরস্থান নির্বাচন করেন আইয়ুব বাচ্চু</a></li>
                                    <li><a href="">জনগণের কাছে ঐক্যফ্রন্টের গ্রহণযোগ্যতা নেই: কাদের</a></li>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="widget">
                            <div class="wmenu">
                                <a href="#">সর্বাধিক পঠিত</a>
                                <ul>
                                    <li><a href="">দারাজ-হাইটেক ফার্নিচারের মধ্যে চুক্তি সই</a></li>
                                    <li><a href="">মৃত্যুর ছয় দিন আগে নিজের কবরস্থান নির্বাচন করেন আইয়ুব বাচ্চু</a></li>
                                    <li><a href="">উলিপুরে বিদ্যুৎস্পৃষ্টে শ্রমিক নিহত</a></li>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="widget">
                            <div class="wmenu">
                                <a href="#">সম্পাদকের বাছাই</a>
                                <ul>
                                    <li><a href="">যোগ্য লোকের অভাবে ড. কামালকে বেছে নিয়েছে বিএনপি: বাণিজ্যমন্ত্রী</a></li>
                                    <li><a href="">রাষ্ট্রপতি রোববার জেনেভা যাচ্ছেন</a></li>
                                    <li><a href="">মায়ের পাশেই চির নিদ্রায় আইয়ুব বাচ্চু</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="newsPopup" class="modal">
                <div class="modal-content customized_content loading_img" id="modal-content">
                    <div class="modal-head" >
                        <table width="100%" class="modal_table">
                            <tr>
                                <td>
                                    <div class="add-this">
                                    </div>
                                </td>
                                <td>
                                    <div class="pull-left">
                                        <p>
                                            <a class="tbl_head_name"href="">epaper.banglardorpon.com</a>
                                        </p>
                                    </div>

                                    <div class="pull-right">
                                        <p>

                                            <button class="btn btn-danger close" style="padding: 6px 10px 6px 10px;margin-top: 2px; margin-top: 2px;margin-right: 10px;"  title="close"><i class="fa fa-times"></i></button>
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="modal-body text-center" style="padding: 20px;">
                        <div class="modal-main-img" id="newsImg" style="overflow-x: auto;">

                            <center>
                                <img src="" class="image_view" id="image_view" style="border: 1px solid #CCC;" alt="epaper.banglardorpon.com" />
                                <img src="" class="related_image" id="related_image" style="border: 1px solid #CCC;display: none" alt="epaper.banglardorpon.com" />
                            </center>

                        </div>

                        <div style="margin-top: 20px;margin-right:auto;">

                            <div style="    float: left;border: 1px solid;padding: 7px;border-radius: 5px;background: #2b2b2b;">
                                <span style="color:wheat;">Share <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></span>&nbsp;


                                <button type="button" style="background-color: #3C5A98"  class="btn btn-default share_on_fb"><i class="fab fa-facebook" style="color: white; font-size:14px;" aria-hidden="true"></i></button>

                                <button type="button" style="background-color: #1DA1F2"  class="btn btn-default share_on_twt"><i class="fab fa-twitter" style="color: white;font-size:14px;" aria-hidden="true"></i></button>

                                <button type="button"  class="btn btn-default share_on_gplus" style="background-color: #E53935"><i class="fab fa-google" style="color: white;font-size:14px;" aria-hidden="true"></i></button>

                                <button type="button" onclick='printDiv("মঙ্গলবার, ২৫ সেপ্টেম্বার ২০১৮");'  name="b_print" class="btn btn-success"> <i class="fa fa-print"></i></button>

                            </div>

                            <div style="float: right">
                                <button  class="btn btn-info trigger-prev prvs next_button" style="display: none"><i class="fa fa-backward" aria-hidden="true"></i>&nbsp;&nbsp;পূর্ববর্তী অংশ</button>
                                <button class="btn btn-info trigger-next nxt next_button" style="display: none">পরবর্তী অংশ&nbsp;&nbsp;<i class="fa fa-forward" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <br/>
                    </div>

                    <div class="modal-footer" >
                        <div class="footer text-center" >
                            <p>© eBanglardorpon {{date('Y')}}. All Rights Reserved.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="main_footer">
                <h3>ভারপ্রাপ্ত সম্পাদক : সাইফুল আলম, প্রকাশক সালমা ইসলাম</h3>
                <p>প্রকাশক কর্তৃক ক-২৪৪ প্রগতি সরণি, কুড়িল (বিশ্বরোড), বারিধারা, ঢাকা-১২২৯ থেকে প্রকাশিত এবং যমুনা প্রিন্টিং এন্ড পাবলিশিং থেকে মুদ্রিত।</p>
                <p>পিএবিএক্স : ৮৪১৯২১১-৫, রিপোর্টিং : ৮৪১৯২২৮, বিজ্ঞাপন : ৮৪১৯২১৬, ফ্যাক্স : ৮৪১৯২১৭, সার্কুলেশন : ৮৪১৯২২৯। ফ্যাক্স : ৮৪১৯২১৮, ৮৪১৯২১৯, ৮৪১৯২২০</p>
                <hr />
                <a href="">Developed by</a>
            </div>
        </div>


    </div>
    <script type="text/javascript" src="<?php echo asset('assets/js/jquery.min.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/maphilight/1.4.0/jquery.maphilight.min.js" integrity="sha512-AXsnvY/qS75ZpZGBz0CkJHMY55DNWyTeXmjZU2W8IZNHcnxSP31UuAaiCWfdajWk+a3kAeSX8VpYLsP635IGuA==" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?php echo asset('assets/js/date-dropdown.js');?>">
    </script>
    <script type="text/javascript">
        $(function () {
            $('.map').maphilight();
        });

        function showmodel(img,related_image,location,image_width,news_type){

            document.getElementById('image_view').src=location+img;

            if(related_image != '' && related_image !=null){

                document.getElementById('related_image').src=location+related_image;
                if (news_type=='1') {
                    $('.nxt').show();
                    $(".nxt").click(function(){
                        $('.image_view').hide();
                        $('.prvs').show();
                        $('.nxt').hide();
                        $('.related_image').show();
                        var modal_width = $('.related_image').width();
                        modal_width=modal_width+50;
                        if(modal_width>1050){
                            modal_width=1050;
                        }
                        if(modal_width<750){
                            modal_width=750;
                        }
                        document.getElementById("modal-content").style.width = modal_width+'px';
                    });

                    $(".prvs").click(function(){
                        $('.prvs').hide();
                        $('.nxt').show();
                        $('.image_view').show();
                        $('.related_image').hide();

                        var modal_width = $('.image_view').width();
                        modal_width=modal_width+50;
                        if(modal_width>1050){
                            modal_width=1050;
                        }
                        if(modal_width<750){
                            modal_width=750;
                        }
                        document.getElementById("modal-content").style.width = modal_width+'px';
                    });

                }else{
                    $('.prvs').show();

                    $(".prvs").click(function(){
                        $('.prvs').hide();
                        $('.nxt').show();
                        $('.image_view').hide();
                        $('.related_image').show();

                        var modal_width = $('.related_image').width();
                        modal_width=modal_width+50;
                        if(modal_width>1050){
                            modal_width=1050;
                        }
                        if(modal_width<750){
                            modal_width=750;
                        }
                        document.getElementById("modal-content").style.width = modal_width+'px';
                    });

                    $(".nxt").click(function(){
                        $('.nxt').hide();
                        $('.prvs').show();
                        $('.related_image').hide();
                        $('.image_view').show();
                        var modal_width = $('.image_view').width();
                        modal_width=modal_width+50;
                        if(modal_width>1050){
                            modal_width=1050;
                        }
                        if(modal_width<750){
                            modal_width=750;
                        }
                        document.getElementById("modal-content").style.width = modal_width+'px';
                    });

                }

            }
            var modal_width =image_width;
            if(modal_width>1050){
                modal_width=1050;
            }

            if(modal_width<750){
                modal_width=750;
            }
            document.getElementById("modal-content").style.width = modal_width+'px';

            document.getElementById('newsPopup').style.display="block";
        }

        $('.close').on('click',function(){
            var remove_image_item = document.getElementsByClassName("image_view")[0].innerHTML = "";
            $(".modal-body .image_view").attr( "src", remove_image_item );

            var remove_related_item = document.getElementsByClassName("related_image")[0].innerHTML = "";
            $(".modal-body .related_image").attr( "src", remove_related_item );

            document.getElementById('newsPopup').style.display="none";
            $('.image_view').show();
            $('.related_image').hide();
            $('.nxt').hide();
            $('.prvs').hide();
        });
        $(document).on('click', '.nbtn', function(){
            var archive_date = $("input[name=date]").val();
            var site_url = $(".site_url").val();

            if(archive_date == ''){
                alert('Please Select A Valid Date !');
                window.reload();
            }

            if(archive_date != null){
                var request_url = 'epaper?date='+archive_date+'&page=1';
                window.location=request_url;
            }
        });
        $(function() {
            $("#datepick").dateDropdowns({required: true});
        });
        var modal = document.getElementById('newsPopup');
        window.onclick = function(event) {
            if (event.target == modal) {
                /*==remove related image class==*/
                var remove_image_item = document.getElementsByClassName("image_view")[0].innerHTML = "";
                $(".modal-body .image_view").attr( "src", remove_image_item );
                var remove_related_item = document.getElementsByClassName("related_image")[0].innerHTML = "";
                $(".modal-body .related_image").attr( "src", remove_related_item );
                $('.image_view').show();
                modal.style.display = "none";
                $('.nxt').hide();
                $('.prvs').hide();
                document.getElementById("body").style.overflow = 'scroll';
            }
        }

        $('.share_on_fb').click(function(){
            var fb_link = '/'+$(".image_view").attr( "src" );
            var splitedfb = fb_link.split("images/");
            var lengthfb = splitedfb.length;
            var fb_link = splitedfb[lengthfb-2];
            var mainImage = splitedfb[lengthfb-1];

            var related_image = $(".related_image").attr( "src" );
            var site_url = $(".site_url").val();
            var current_date = $(".current_date").val();

            if(related_image != ''){
                var splited = related_image.split("/");
                var length = splited.length;
                var related_image = splited[length-1];
                var requested_url = site_url+fb_link+'images/shared/'+mainImage+'/'+related_image;
                window.open('https://www.facebook.com/sharer/sharer.php?u='+requested_url, '', 'window settings');
            }else{
                var requested_url = site_url+fb_link+'images/shared/'+mainImage;
                window.open('https://www.facebook.com/sharer/sharer.php?u='+requested_url, '', 'window settings');
            }
        });

        $('.share_on_twt').click(function(){
            var tw_link = '/'+$(".image_view").attr( "src" );
            var tw_splited = tw_link.split("images/");
            var tw_length = tw_splited.length;
            var tw_link = tw_splited[tw_length-2];
            var tw_mainImage = tw_splited[tw_length-1];

            var tw_related_image = $(".related_image").attr( "src" );
            var site_url = $(".site_url").val();
            var current_date = $(".current_date").val();

            if(tw_related_image != ''){
                var tw_related_splited = tw_related_image.split("/");
                var tw_related_length = tw_related_splited.length;
                var tw_related_image = tw_related_splited[tw_related_length-1];
                var tw_requested_url = site_url+tw_link+'images/shared/'+tw_mainImage+'/'+tw_related_image;
                window.open('https://www.twitter.com/share?url='+tw_requested_url, '', 'window settings');
            }else{
                var tw_requested_url = site_url+tw_link+'images/shared/'+tw_mainImage;
                window.open('https://www.twitter.com/share?url='+tw_requested_url, '', 'window settings');
            }

        });


    </script>
@endsection
