@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="row">
                        <div class="col-12">
                            <div class="col-md-6 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Edit Category</h4>
                                        <form class="forms-sample" action="{{route('categories.update',['category'=>$category->id])}}" method="post">
                                            {{method_field('put')}}
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                            @if($category->parent_id !=0)
                                            <div class="form-group"  id="parent_section">
                                                <label>Parent Category</label>
                                                {!! Form::select('parent_id',[''=>'Select Category'] +$activeCategories, $category->parent_id, $attributes = array('class'=>'js-example-basic-single form-control',
                           'id'=>"parent_category")) !!}
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="exampleInputName1">Name</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$category->name}}">
                                            </div>

                                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                                            <a class="btn btn-light" href="{{route('categories.index')}}">Cancel</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-script')
    <script>
        $('#category_type').on('change',function () {
            if ($(this).is(':checked')){
                $('#parent_section').slideDown();
            }else{
                $('#parent_category').val('');
                $('#parent_section').slideUp();
            }
        });
    </script>
@endsection
